from flask import Flask, jsonify
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from flask_cors import CORS
from flask_restplus import Api

db = SQLAlchemy()
api: Api = Api(title='Gambit Accepted', prefix='/api', doc='/api/doc')


def create_app(env=None):
    from app.config import config_by_name
    from app.routes import register_routes

    app = Flask(__name__)
    app.config.from_object(config_by_name[env or 'dev'])
    if env == 'dev' or env == 'test':
        CORS(app)
    api.init_app(app)

    register_routes(api, app)
    db.init_app(app)
    migrate = Migrate(app, db)

    @app.route('/health')
    def health():
        return jsonify('healthy')

    return app
