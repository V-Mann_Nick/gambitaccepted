from pytest import fixture
from .model import Player
from .interface import PlayerInterface


@fixture
def interface() -> PlayerInterface:
    return {
        'first_name': 'test',
        'middle_names': 'test',
        'last_name': 'test'
    }

def test_PlayerInterface_create(interface: PlayerInterface):
    assert interface

def test_PlayerInterface_works(interface: PlayerInterface):
    player = Player(**interface)
    assert player
