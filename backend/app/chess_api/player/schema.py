from marshmallow import fields, Schema


class PlayerSchema(Schema):
    """For serializing player object"""

    id = fields.Integer()
    firstName = fields.String(attribute='first_name')
    middleNames = fields.String(attribute='middle_names')
    lastName = fields.String(attribute='last_name')
    gameIds = fields.Nested('GameSchema', attribute='games', only=('id',), many=True, dump_only=True)

    class Meta:
        ordered = True
