from typing import List, Set, Dict, Tuple

from flask import request
from flask_restplus import Namespace, Resource, abort
from flask_accepts import responds, accepts
from marshmallow import ValidationError
from flask_sqlalchemy import Pagination

from app.chess_api.game.schema import GameSchemaList, GameSchemaQuery
from app.chess_api.game.service import GameService
from .service import PlayerService
from .model import Player
from .schema import PlayerSchema


ns = Namespace('Player')


@ns.route('/')
class PlayerResoure(Resource):

    @accepts(dict(name='name', type=str), api=ns)
    @responds(schema=PlayerSchema(exclude=('gameIds',), many=True))
    @ns.doc(
        'get all players or query for them',
        params={
            'name': 'name of player'
        }
    )
    @ns.response(200, 'Success')
    @ns.response(404, 'Not found')
    def get(self) -> List[Player]:
        """Returns queried players or all of them"""

        name_query: str = request.parsed_args.get('name')
        if name_query:
            players = PlayerService.get_by_name(name_query)
            if players:
                return PlayerService.get_by_name(name_query)
            else:
                abort(404)
        else:
            return PlayerService.get_all()


@ns.route('/<int:id>')
class PlayerIdResource(Resource):

    @responds(schema=PlayerSchema)
    @ns.response(200, 'Sucess')
    @ns.response(404, 'Not found')
    def get(self, id) -> Player:
        """Returns player by id (inclduing played game ids)"""

        player = PlayerService.get_by_id(id)
        if player:
            return player
        else:
            abort(404)


@ns.route('/<int:id>/games')
class PlayerGamesResource(Resource):

    @accepts(
        dict(name='pageSize', type=int, default=10),
        dict(name='pageNumber', type=int, default=1),
        dict(name='playerColor', type=str),
        dict(name='opponent', type=str),
        dict(name='opponentColor', type=str),
        dict(name='event', type=str),
        dict(name='site', type=str),
        dict(name='whiteElo', type=str),
        dict(name='blackElo', type=str),
        dict(name='result', type=str),
        api=ns
    )
    @responds(schema=GameSchemaList)
    @ns.doc(
        params={
            'pageSize': 'games per page (1-20)',
            'pageNumber': 'current page number',
            'playerColor': 'color of player ("w", "white", "b", "black")',
            'opponent': 'opponent',
            'opponentColor': 'color of second player ("w", "white", "b", "black")',
            'event': 'tournament or event of some sorts',
            'site': 'location or website',
            'whiteElo': 'lower or greater than some number (e.g. ">2000", "<3000")',
            'blackElo': 'lower or greater than some number (e.g. ">2000", "<3000")',
            'result': 'result of game ("1-0", "0-1" or "1/2-1/2")'})
    @ns.response(200, 'Sucess')
    @ns.response(400, 'Bad request')
    @ns.response(404, 'Not found')
    def get(self, id) -> Dict:
        """With player as context queries games based on params"""

        # translate arguments for GameSchemaQuery
        params = request.parsed_args
        color: str = request.parsed_args.get('playerColor')
        args_translated = {
            'pageSize': params.get('pageSize'),
            'pageNumber': params.get('pageNumber'),
            'player2': params.get('opponent'),
            'player2Color': params.get('opponentColor'),
            'event': params.get('event'),
            'site': params.get('site'),
            'whiteElo': params.get('whiteElo'),
            'blackElo': params.get('blackElo'),
            'result': params.get('result')
        }

        # validate parameters
        try:
            args_none_removed: Dict = {
                k: v for k, v in args_translated.items() if v is not None}
            query_params: Dict = GameSchemaQuery().load(args_none_removed)
        except ValidationError as e:
            abort(400, **e.messages)

        result: Pagination = PlayerService.get_games(
            id,
            query_params,
            color=color[0] if color else ''
        )

        if result.items:
            return {
                'foundGames': result.total,
                'pageSize': result.per_page,
                'pageNumber': result.page,
                'pageTotal': result.pages,
                'games': result.items
            }
        else:
            abort(404)
