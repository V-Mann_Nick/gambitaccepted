from typing import TypedDict


class PlayerInterface(TypedDict):
    first_name: str
    middle_names: str
    last_name: str
