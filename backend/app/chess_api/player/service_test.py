from typing import List, Tuple, Union
from pytest import fixture

from flask_sqlalchemy import SQLAlchemy

from app.test.fixtures import app, db
from .model import Player
from .service import PlayerService
from .interface import PlayerInterface


def test_get_all(db: SQLAlchemy):
    player1: Player = Player(first_name='test1', middle_names='test1', last_name='test1')
    player2: Player = Player(first_name='test2', middle_names='test2', last_name='test2')
    db.session.add(player1)
    db.session.add(player2)
    db.session.commit()

    results: List[Player] = PlayerService.get_all()

    assert len(results) == 2
    assert player1 in results and player2 in results


def test_get_by_name(db: SQLAlchemy):
    player1: Player = Player(first_name='Frank', middle_names='Middle', last_name='Man')
    player2: Player = Player(first_name='Jane', middle_names='Joslin', last_name='Jank')
    player3: Player = Player(first_name='Josi', middle_names='Mary', last_name='Jank')
    db.session.add(player1)
    db.session.add(player2)
    db.session.add(player3)
    db.session.commit()

    result: List[Player] = PlayerService.get_by_name('Jank')
    assert player2 in result and player3 in result

    result: List[Player] = PlayerService.get_by_name('Frank Middle Man')
    assert player1 in result

    result: List[Player] = PlayerService.get_by_name('Jane Jank')
    assert player2 in result


def test_update(db: SQLAlchemy):
    player: Player = Player(first_name='test', middle_names='test', last_name='test')
    db.session.add(player)
    db.session.commit()

    updates: PlayerInterface = dict(last_name='new test')
    PlayerService.update(player, updates)

    result: Player = Player.query.get(player.id)
    assert result.last_name == 'new test'


def test_create(db: SQLAlchemy):
    player: PlayerInterface = dict(first_name='test', middle_names='test', last_name='test')
    PlayerService.create(player)
    results: List[Player] = Player.query.all()

    assert len(results) == 1
    for key in player.keys():
        assert getattr(results[0], key) == player[key]


def test_determine(db: SQLAlchemy):
    # test player not found
    player: Union[Player, None] = PlayerService.determine('Bruce Banner', create=True)
    assert len(Player.query.all()) == 1

    # test middle name update
    result: Union[Player, None] = PlayerService.determine('Bruce Hulk Banner', create=True)
    assert player is result
    assert player.middle_names == 'Hulk'

    # test matching middle names
    player: Union[Player, None] = result  # 'Bruce Hulk Banner'
    result: Union[Player, None] = PlayerService.determine('Bruce Hulk Banner', create=True)
    assert player is result
    player1: Union[Player, None] = result  # 'Bruce Hulk Banner'

    # test different middle names
    result: Union[Player, None] = PlayerService.determine('Bruce Green Man Banner', create=True)
    assert player is not result
    assert len(Player.query.all()) == 2
    player2: Union[Player, None] = result  # 'Bruce Green Man Banner'

    # test matching middle names again with ambigious results
    result: Union[Player, None] = PlayerService.determine('Bruce Hulk Banner', create=True)
    assert player1 is result
    result: Union[Player, None] = PlayerService.determine('Bruce Green Man Banner', create=True)
    assert player2 is result

    # test no middle name match with ambigious result
    result: Union[Player, None] = PlayerService.determine('Bruce Green Power Banner', create=True)
    assert len(Player.query.all()) == 3

    # test no create
    result: Union[Player, None] = PlayerService.determine('Clark Kent')
    assert len(Player.query.all()) == 3
    assert result == None


def test_split_name():
    name1: str = 'Donald Duck'
    name2: str = 'Duck, Donald'
    name3: str = 'Donald Walt Disney Duck'
    name4: str = 'Duck, Donald Walt Disney'

    name1: Tuple = PlayerService.split_name(name1)
    name2: Tuple = PlayerService.split_name(name2)
    name3: Tuple = PlayerService.split_name(name3)
    name4: Tuple = PlayerService.split_name(name4)

    assert name1[0] == 'Donald' and name1[1] == '' and name1[2] == 'Duck'
    assert name2[0] == 'Donald' and name2[1] == '' and name3[2] == 'Duck'
    assert name3[0] == 'Donald' and name3[1] == 'Walt Disney' and name3[2] == 'Duck'
    assert name4[0] == 'Donald' and name4[1] == 'Walt Disney' and name4[2] == 'Duck'
