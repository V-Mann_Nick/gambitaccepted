from app import db
from sqlalchemy import Integer, Column, String
from sqlalchemy.orm import relationship
from sqlalchemy.schema import ForeignKey
from .interface import PlayerInterface



class Pairing(db.Model):
    id = Column(Integer, primary_key=True)
    game_id = Column(Integer, ForeignKey('game.id'))
    player_id = Column(Integer, ForeignKey('player.id'))
    color = Column(String(1))  # either 'w' or 'b'

    def __repr__(self):
        return f'<{"White" if self.color == "w" else "Black"}: {Player.query.get(self.player_id)}>'


class Player(db.Model):
    id = Column(Integer, primary_key=True)
    first_name = Column(String(64), index=True)
    middle_names = Column(String(64), index=True)
    last_name = Column(String(64), index=True)
    games = relationship('Game', secondary='pairing', back_populates='players', lazy='dynamic')

    def __repr__(self):
        return f'<Player: {self.first_name} {self.last_name}>'

    def __eq__(self, other):
        return \
            self.first_name == other.first_name and \
            self.middle_names == other.middle_names and \
            self.last_name == other.last_name

    def __ne__(self, other):
        return \
            self.first_name != other.first_name or \
            self.middle_names != other.middle_names or \
            self.last_name != other.last_name

    def full_name(self) -> str:
        # return f'{self.first_name} {f"{self.middle_name} " if self.middle_name else ""}{self.last_name}'
        return '{}{}{}'.format(
            self.first_name,
            f' {self.middle_names}' if self.middle_names else '',
            f' {self.last_name}' if self.last_name else ''
        )

    def update(self, changes: PlayerInterface):
        for key, val in changes.items():
            setattr(self, key, val)
        return self
