from typing import List, Tuple, Optional, Union, Dict

from flask_sqlalchemy import BaseQuery, Pagination
from sqlalchemy.orm import aliased
from sqlalchemy.orm.util import AliasedClass

from app.chess_api.game.model import Game
from app.chess_api.game.schema import GameSchemaQuery
from app import db
from .model import Player, Pairing
from .interface import PlayerInterface


class PlayerService:
    @staticmethod
    def get_all() -> List[Player]:
        """Returns all players"""

        return Player.query.all()

    @staticmethod
    def get_by_id(id: int) -> Player:
        """Returns player by id"""

        return Player.query.get(id)

    @staticmethod
    def get_by_name(name: str) -> List[Player]:
        """Returns list of player based on a string"""

        name = name.strip()  # who knows....
        if len(name.split(' ')) > 1:  # at least first name and last name
            first_name, middle_names, last_name = PlayerService.split_name(name)
            query: BaseQuery = Player.query.filter(
                Player.first_name.contains(first_name),  # and
                Player.last_name.contains(last_name)
            )
            if middle_names:
                query: BaseQuery = Player.query.filter(Player.middle_names.contains(middle_names))
        else:  # only one name. Can be any of the three
            query: BaseQuery = Player.query.filter(
                Player.first_name.contains(name) |  # or
                Player.middle_names.contains(name) |  # or
                Player.last_name.contains(name)
            )
        return query.all()


    @staticmethod
    def get_games(player: Union[Player, int], query_params: Dict, color: Optional[str]) -> Pagination:
        """Returns games of player giving option to filter with query.
        query_params is loaded via GameSchemaQuery."""

        from app.chess_api.game.service import GameService  # prevent circular imports

        # check type of player
        player_id: int = player if type(player) == int else player.id

        # create query context
        alias_pairing: AliasedClass = aliased(Pairing)
        alias_player: AliasedClass = aliased(Player)
        query: BaseQuery = Game.query\
            .join(alias_pairing, Game.id == alias_pairing.game_id)\
            .join(alias_player, alias_pairing.player_id == alias_player.id)\
            .filter(alias_player.id == player_id)
        if color:
            query = query.filter(alias_pairing.color == color)

        return GameService.query(query_params, context=query)


    @staticmethod
    def update(player: Player, player_change_update: PlayerInterface) -> Player:
        """Updates player via PlayerInterface"""

        player.update(player_change_update)
        db.session.commit()
        return player

    @staticmethod
    def create(new_attrs: PlayerInterface) -> Player:
        """Creates player object and commits to db"""

        new_player = Player(
            first_name=new_attrs.get('first_name') or None,
            middle_names=new_attrs.get('middle_names') or None,
            last_name=new_attrs.get('last_name') or None
        )

        db.session.add(new_player)
        db.session.commit()

        return new_player

    @staticmethod
    def determine(name: str, create: Optional[bool]=False) -> Union[Player, None]:
        """Finds or creates player (if create=True) in db
        otherwise just finds and returns Player or None"""

        first_name, middle_names, last_name = PlayerService.split_name(name)
        player: BaseQuery = Player.query.filter_by(first_name=first_name, last_name=last_name)

        if player.count() > 1:  # first name and last name yield ambigious results
            player: BaseQuery = Player.query.filter_by(
                first_name=first_name,
                middle_names=middle_names,
                last_name=last_name
            )
            if player.count() > 0:
                player: Player = player.first()
            else:
                if create:
                    player: Player = PlayerService.create(PlayerInterface(
                        first_name=first_name,
                        middle_names=middle_names,
                        last_name=last_name
                    ))
                else:
                    player = None
        elif player.count() == 1:
            player: Player = player.first()
            # if found player has no middle names but middle names were given
            # then add the middle names to the player and commit to db
            if not player.middle_names and middle_names:
                if create:
                    PlayerService.update(player, PlayerInterface(middle_names=middle_names))
            # if found players middle names are not equal to given middle names
            # create new player
            elif player.middle_names and player.middle_names != middle_names:
                if create:
                    player: Player = PlayerService.create(PlayerInterface(
                        first_name=first_name,
                        middle_names=middle_names,
                        last_name=last_name
                    ))
        else:
            if create:
                player: Player = PlayerService.create(PlayerInterface(
                    first_name=first_name,
                    middle_names=middle_names,
                    last_name=last_name
                ))
            else:
                player = None
        return player

    @staticmethod
    def split_name(name: str) -> Tuple[str]:
        """Splits name given in comma-format or simple-format"""

        if ',' in name:  # example: Ford, Henry Markus
            names: List = [part.strip() for part in name.split(',')]
            last_name: str = names[0]  # -> 'Ford'
            after_comma: List = names[1].split(' ')
            first_name: str = after_comma[0]  # -> 'Henry'
            middle_names: str = ' '.join(after_comma[1:]) if len(after_comma) > 1 else ''  # -> 'Markus'
        else:  # example: Henry Markus Ford
            names: List = [part.strip() for part in name.split(' ')]  # making sure no extra blanks
            first_name: str = names[0]
            last_name: str = names[-1] if len(names) > 1 else ''
            middle_names: str = ' '.join(names[1:-1]) if len(names) > 2 else ''
        return first_name, middle_names, last_name
