from unittest.mock import patch

from flask.testing import FlaskClient

from app.test.fixtures import client, app  # noqa
from .model import Player
from .service import PlayerService
from .schema import PlayerSchema
from .. import BASE_ROUTE


def make_player(name: str) -> Player:
    first_name, middle_names, last_name = PlayerService.split_name(name)
    return Player(
        first_name=first_name,
        middle_names=middle_names,
        last_name=last_name
    )


class TestPlayerResource:
    endpoint: str = f'/api/{BASE_ROUTE}/player/'

    @patch.object(
        PlayerService,
        'get_all',
        lambda: [
            make_player('Magnus Carlsen'),
            make_player('Hikaru Nakamura'),
            make_player('Nicklas David Sedlock')
        ]
    )
    def test_get(self, client: FlaskClient):
        with client:
            expected = PlayerSchema(many=True, exclude=('gameIds',)).dump([
                make_player('Magnus Carlsen'),
                make_player('Hikaru Nakamura'),
                make_player('Nicklas David Sedlock')
            ])
            results = client.get(self.endpoint, follow_redirects=True).get_json()
            for result in results:
                assert result in expected


class TestPlayerIdResource:
    endpoint: str = f'/api/{BASE_ROUTE}/player/'

    @patch.object(
        PlayerService,
        'get_by_id',
        lambda id: make_player('Magnus Carlsen')
    )
    def test_get(self, client: FlaskClient):
        with client:
            expected = PlayerSchema().dump(make_player('Magnus Carlsen'))
            result = client.get(f'{self.endpoint}1', follow_redirects=True).get_json()
            assert result == expected

