from pytest import fixture
from .model import Pairing, Player


@fixture
def pairing():
    return Pairing(
        game_id=0,
        player_id=0,
        color='w'
    )


@fixture
def player():
    return Player(
        first_name='test',
        middle_names='test',
        last_name='test'
    )


def test_Pairing_create(pairing):
    assert pairing


def test_Player_create(player):
    assert player
