from pytest import fixture
from typing import Dict

from flask_sqlalchemy import SQLAlchemy

from app.test.fixtures import app, db
from app.test.fixtures_openings import sample_openings, db_with_openings
from app.chess_api.game.service import GameService
from app.chess_api.game.model import Game
from .model import Player
from .schema import PlayerSchema
from .interface import PlayerInterface


@fixture
def schema() -> PlayerSchema:
    return PlayerSchema()


@fixture
def game(db_with_openings: SQLAlchemy) -> Game:
    pgn = '\n'.join([
        '[Event "43. Olympiad 2018"]',
        '[Site "Batumi GEO"]',
        '[Date "2018.10.03"]',
        '[Round "9.18"]',
        '[Result "1/2-1/2"]',
        '[White "Dimitrios Mastrovasilis"]',
        '[Black "Nodirbek Yakubboev"]',
        '[ECO "C26"]',
        '',
        '1. e4 e5 2. Nc3 Nf6 3. d4 d6 4. dxe5 dxe5 5. Qxd8+ Kxd8 6. Bc4 Bb4 7. Bxf7 Rf8',
        '8. Bb3 Nxe4 9. Ne2 Bf5 10. Be3 Nd7 11. O-O-O Nxc3 12. Nxc3 c6 13. Ne2 Kc7 14.',
        'Ng3 Bg6 15. c3 Be7 16. Bc2 Bxc2 17. Kxc2 Nf6 18. Rhe1 Nd5 19. Ne4 Rad8 20. Rd3',
        'h6 21. Bd2 b6 22. a3 Bh4 23. c4 Nf6 24. Rxd8 Kxd8 25. g3 Nxe4 26. Rxe4 Be7 27.',
        'Be3 Bf6 28. a4 Kd7 29. a5 Bg5 30. Bxg5 hxg5 31. Rxe5 Rxf2+ 32. Kb3 Rxh2 33.',
        'Rxg5 Rg2 34. a6 b5 35. Rxg7+ Kd6 36. cxb5 cxb5 37. Kc3 Kc5 38. b4+ Kd5 39. Kd3',
        'Ra2 40. Rg5+ Ke6 41. Kd4 Kf6 42. Rxb5 Rxa6 43. Kc5 Re6 44. Rb7 Ra6 45. Rb5 Re6',
        '46. Rb7 Ra6 47. Rc7 Kf5 48. Kb5 Ra3 49. Kc4 Kg4 50. Rg7+ Kf5 51. Kb5 a6+ 52.',
        'Kc4 Ra1 53. Rd7 Kg4 54. Rd3 Ra4 55. Kb3 Ra1 56. Rc3 Kh3 57. Kb2 Ra4 58. Kb3 Ra1',
        '59. Kc4 Ra4 60. Kc5 Kg4 61. Rd3 Kh3 62. Rb3 Kg4 63. Rb1 Kxg3 64. Kc4 a5 65. Kb5',
        'Rxb4+ 66. Rxb4 axb4 67. Kxb4 1/2-1'
    ])
    return GameService.create(dict(pgn=pgn))


def test_PlayerSchema_create(schema: PlayerSchema):
    assert schema


def test_PlayerSchema_serialize(schema: PlayerSchema, game: Game):
    player: Player = game.players[0]
    result: Dict = schema.dump(player)
    assert result.get('firstName') == 'Dimitrios'
    assert result.get('middleNames') == None
    assert result.get('lastName') == 'Mastrovasilis'
    assert {'id': game.id } in result.get('gameIds')



def test_PlayerSchema_deserialize(schema: PlayerSchema):
    params: PlayerInterface = schema.load({
        'firstName': 'Test',
        'middleNames': 'User',
        'lastName': 'Name'
    })
    player: Player = Player(**params)

    assert player.first_name == 'Test'
    assert player.middle_names == 'User'
    assert player.last_name == 'Name'
