from marshmallow import Schema, fields, validate, validates_schema, ValidationError


class PrinterSchemaOptions(Schema):
    """For deserializing and validating printer options"""

    pgn = fields.String()
    gameId = fields.Integer(attribute='game_id')
    elementsArrange = fields.String(
        attribute='elements_arrange',
        validate=[validate.OneOf([
            'board-move-comment',
            'move-board-comment',
            'move-comment-board'
        ])]
    )
    halfmovesToBePrinted = fields.List(
        fields.Integer(validate=[validate.Range(min=0)]),
        attribute='halfmoves_to_be_printed'
    )
    darkTileColor = fields.String(
        attribute='dark_tile_color',
        validate=[validate.Regexp(r'^#(\w|\d){6}$')]
    )
    lightTileColor = fields.String(
        attribute='light_tile_color',
        validate=[validate.Regexp(r'^#(\w|\d){6}$')]
    )
    pageFormat = fields.String(
        attribute='page_format',
        validate=[validate.OneOf(['A4', 'letter'])]
    )
    pageMargin = fields.Float(
        attribute='page_margin',
        validate=[validate.Range(min=0.1, max=5.0)]
    )
    fontName = fields.String(
        attribute='font_name',
        validate=[validate.OneOf(['Helvetica', 'Courier', 'Times-Roman'])]
    )
    fontSize = fields.Integer(
        attribute='font_size',
        validate=[validate.Range(min=5, max=30)]
    )
    colGap = fields.Float(
        attribute='col_gap',
        validate=[validate.Range(min=0.1, max=5.0)]
    )

    @validates_schema
    def validate_gameSource(self, data, **kwargs):
        if data.get('pgn') and data.get('game_id'):
            raise ValidationError('only either pgn or id can be source of game')
