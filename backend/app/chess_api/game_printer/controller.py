from typing import Dict

from flask import request, Response, make_response
from flask_restplus import Namespace, Resource
from flask_accepts import accepts

from .service import PrinterService
from .schema import PrinterSchemaOptions
from ..game.service import GameService


ns = Namespace('Printer')


@ns.route('/')
class PrinterResource(Resource):

    @accepts(schema=PrinterSchemaOptions, api=ns)
    @ns.doc('post pgn (escape parentheses!) and options for creating pdf')
    def post(self) -> Response:
        arguments = request.parsed_obj
        if arguments.get('game_id'):
            pgn = GameService.get_pgn(arguments.get('game_id'))
            del arguments['game_id']
            arguments['pgn'] = pgn
        printer: PrinterService = PrinterService(**arguments)
        document: Dict = printer.build_document()
        response: Response = Response(document['file'].getvalue())
        response.headers['Content-Disposition']\
            = f'attachment; filename="{document["file_name"]}.pdf"'
        response.mimetype = 'application/pdf'
        return response
