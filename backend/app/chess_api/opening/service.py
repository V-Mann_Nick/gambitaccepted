from typing import List, Optional, Union, Tuple, Dict
from io import StringIO

from chess.pgn import GameNode, read_game
from flask_sqlalchemy import BaseQuery

from app import db
from .model import Opening
from .interface import OpeningInterface
from .schema import OpeningSchemaQuery


class OpeningService:
    @staticmethod
    def get_all() -> List[Opening]:
        """Return all Openings"""

        return Opening.query.all()

    @staticmethod
    def get_by_id(id: int) -> Opening:
        """Returns opening by id"""

        return Opening.query.get(id)

    @staticmethod
    def query(params: Dict) -> List[Opening]:
        """Queries database with given params. These are loaded via OpeningSchemaQuery
        params:
            name: str
            eco_code: str
        """
        name: str = params.get('name')
        eco_code: str = params.get('eco_code')

        query: BaseQuery = Opening.query
        if name:
            query = query.filter(
                Opening.name.contains(name) |  # or
                Opening.variation.contains(name)
            )
        if eco_code:
            query = query.filter(Opening.eco_code.contains(eco_code))

        return query.all()

    @staticmethod
    def update(
            opening: Opening,
            opening_change_update: OpeningInterface) -> Opening:
        """Update Opening via OpeningInterface"""

        opening.update(opening_change_update)
        db.session.commit()
        return opening

    @staticmethod
    def best_match(
            game: GameNode, openings: List[Opening]) -> Union[Opening, None]:
        """With a list of openings as context determines opening of game"""

        longest: Tuple = (0, None)  # (move_count, Opening-object)
        for opening in openings:
            fen: str = opening.final_position
            fen_parts: List[str] = fen.split(' ')
            # example: 'rnbqkbnr/pp1ppppp/8/8/4P3/p7/2PP1PPP/RNBQKBNR w KQkq - 0 4'
            num_moves: int = 2 * \
                int(fen_parts[-1]) - (2 if fen_parts[-5] == 'w' else 1)
            if num_moves > longest[0]:
                for i, move in enumerate(game.mainline()):
                    if i + 1 == num_moves:  # stop when at same move number
                        if fen == move.board().fen():  # compare position
                            longest: Tuple = (num_moves, opening)
        return longest[1]

    @staticmethod
    def determine(game: Union[GameNode, str]) -> Union[Opening, None]:
        """Based on eco determines list of openings to be checked and calls best_match()"""

        if isinstance(game, str):
            game = read_game(StringIO(game))
        eco_matches: BaseQuery = Opening.query.filter_by(
            eco_code=game.headers.get('ECO'))
        best_match: Union[Opening, None] = None
        if eco_matches.count():  # if ECO is ambigious it will explore the moves for a match
            best_match: Union[Opening, None] = OpeningService.best_match(
                game, eco_matches)
        # best_match is none if no eco_match was found or best_matching_opening
        # found nothing
        if not best_match:
            best_match: Union[Opening, None] = OpeningService.best_match(
                game, Opening.query.all())
        return best_match
