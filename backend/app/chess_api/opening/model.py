from sqlalchemy import Integer, Column, String
from sqlalchemy.orm import relationship

from app import db
from .interface import OpeningInterface


class Opening(db.Model):
    id = Column(Integer, primary_key=True)
    eco_code = Column(String(4), index=True)
    name = Column(String(64), index=True)
    variation = Column(String(64))
    moves = Column(String(64), index=True, unique=True)
    final_position = Column(String(32))
    games = relationship('Game', backref='opening', lazy='dynamic')

    def __repr__(self):
        return f'<{self.eco_code}: {self.name}{" {}".format(self.variation) if self.variation else ""}>'

    def update(self, changes: OpeningInterface):
        for key, val in changes.items():
            setattr(self, key, val)
        return self
