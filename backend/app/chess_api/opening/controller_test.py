from unittest.mock import patch
from typing import List

from flask.testing import FlaskClient

from app.test.fixtures import client, app  # noqa
from .model import Opening
from .service import OpeningService
from .schema import OpeningSchema
from .. import BASE_ROUTE


def get_openings() -> List[Opening]:
    openings = list()
    openings.append(Opening(
        eco_code='C26',
        name="Vienna",
        variation="Falkbeer variation",
        moves="1. e4 e5 2. Nc3 Nf6 *",
        final_position='rnbqkb1r/pppp1ppp/5n2/4p3/4P3/2N5/PPPP1PPP/R1BQKBNR w KQkq - 2 3'
    ))
    openings.append(Opening(
        eco_code='C26',
        name="Vienna game",
        moves="1. e4 e5 2. Nc3 Nf6 3. Bc4 *",
        final_position='rnbqkb1r/pppp1ppp/5n2/4p3/2B1P3/2N5/PPPP1PPP/R1BQK1NR b KQkq - 3 3'
    ))
    openings.append(Opening(
        eco_code='B07',
        name="Pirc",
        moves="1. e4 d6 2. d4 Nf6 3. Nc3 c6 *",
        final_position='rnbqkb1r/pp2pppp/2pp1n2/8/3PP3/2N5/PPP2PPP/R1BQKBNR w KQkq - 0 4'
    ))
    return openings


class TestOpeningResource:
    endpoint: str = f'/api/{BASE_ROUTE}/opening/'

    @patch.object(
        OpeningService,
        'query',
        lambda x: get_openings()
    )
    def test_get(self, client: FlaskClient):
        with client:
            expected = OpeningSchema(many=True, exclude=('gameIds',)).dump(get_openings())

            # test get all
            results = client.get(self.endpoint, follow_redirects=True).get_json()
            for result in results:
                assert result in expected

            # # test query name
            # results = client.get(
            #     self.endpoint,
            #     query_string=dict(eco='C26'),
            #     follow_redirects=True
            # ).get_json()
            # for result in results:
            #     assert result in expected[:2]

            # # test query name and eco
            # results = client.get(
            #     self.endpoint,
            #     query_string=dict(name='Vienna game', eco='C26'),
            #     follow_redirects=True
            # ).get_json()
            # for result in results:
            #     assert result in expected[1:2]

    @patch.object(
        OpeningService,
        'determine',
        lambda pgn: get_openings()[2]
    )
    def test_post(self, client: FlaskClient):
        with client:
            pgn = '1. e4 d6 2. d4 Nf6 3. Nc3 c6 4. Nf3 g6 5. h3 Bg7 6. Be3 O-O'
            result = client.post(
                self.endpoint,
                json=dict(pgn=pgn)
            ).get_json()
            expected = OpeningSchema().dump(get_openings()[2])
            assert result == expected


class TestOpeningIdResource:
    endpoint: str = f'/api/{BASE_ROUTE}/opening/'

    @patch.object(
        OpeningService,
        'get_by_id',
        lambda id: get_openings()[id]
    )
    def get(self, client: FlaskClient):
        with client:
            result = client.get(f'{endpoint}1', follow_redirects=True).get_json()
            expected = OpeningSchema().dump(get_openings()[1])
            assert result == expected

