from pytest import fixture

from .model import Opening
from .interface import OpeningInterface


@fixture
def interface() -> OpeningInterface:
    return {
        'eco_code': 'test',
        'name': 'test',
        'variation': 'test',
        'moves': 'test',
        'final_position': 'test'
    }

def test_PlayerInterface_create(interface: OpeningInterface):
    assert interface

def test_PlayerInterface_works(interface: OpeningInterface):
    opening = Opening(**interface)
    assert opening
