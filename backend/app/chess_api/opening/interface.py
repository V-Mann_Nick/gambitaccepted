from typing import TypedDict


class OpeningInterface(TypedDict):
    eco_code: str
    name: str
    variation: str
    moves: str
    final_position: str  # fen-code
