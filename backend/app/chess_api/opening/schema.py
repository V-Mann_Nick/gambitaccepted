from marshmallow import Schema, fields, validate, ValidationError, validates_schema


class OpeningSchema(Schema):
    """For serializing Opening object"""

    id = fields.Integer()
    ecoCode = fields.String(attribute='eco_code')
    name = fields.String()
    variation = fields.String()
    moves = fields.String()
    finalPosition = fields.String(attribute='final_position')
    gameIds = fields.Nested('GameSchema', attribute='games', only=('id',), many=True)

    class Meta:
        ordered = True


class OpeningSchemaDetermine(Schema):
    """For deserializing pgns"""

    pgn = fields.String()


class OpeningSchemaQuery(Schema):
    """For deserializing and validating queries for Openings"""

    name = fields.String(validate=[
        validate.Length(max=64)
    ])
    # match examples: 'C24', 'A00'
    # no match: 'F20', 'A245'
    eco = fields.String(attribute='eco_code', validate=[
        validate.Regexp('^[A-E]\d{2}$')
    ])
