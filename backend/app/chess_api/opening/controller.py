from typing import List, Dict

from flask import request
from flask_restplus import Namespace, Resource, abort
from flask_accepts import responds, accepts
from marshmallow import ValidationError

from .model import Opening
from .service import OpeningService
from .schema import OpeningSchema, OpeningSchemaDetermine, OpeningSchemaQuery


ns = Namespace('Opening')


@ns.route('/')
class OpeningResource(Resource):
    @accepts(
        dict(name='name', type=str),
        dict(name='eco', type=str),
        api=ns
    )
    @responds(schema=OpeningSchema(many=True, exclude=('gameIds',)))
    @ns.doc(
        params={
            'name': 'search for name (optional)',
            'eco': 'and/or search for eco (optional)'
        }
    )
    @ns.response(200, 'Success')
    @ns.response(400, 'Bad Request')
    @ns.response(404, 'Not found')
    def get(self) -> List[Opening]:
        """Returns queried opening(s) or all of them"""

        # validate parameters
        try:
            args_none_removed: Dict = {
                k: v for k, v in request.parsed_args.items() if v is not None
            }
            query_params: Dict = OpeningSchemaQuery().load(args_none_removed)
        except ValidationError as e:
            abort(400, **e.messages)

        openings = OpeningService.query(query_params)
        if openings:
            return openings
        else:
            abort(404)

    @accepts(schema=OpeningSchemaDetermine, api=ns)
    @responds(schema=OpeningSchema)
    @ns.response(200, 'Success')
    @ns.response(400, 'Bad Request')
    @ns.response(404, 'Not found')
    def post(self) -> Opening:
        """Returns the determined opening based on pgn or moves (escape parenthesis!)"""

        opening = OpeningService.determine(request.parsed_obj.get('pgn'))
        if opening:
            return opening
        else:
            abort(404)


@ns.route('/<int:id>')
class OpeningIdResouce(Resource):

    @ns.doc('get opening by id (including played game ids)',
            params={'id': 'id of an opening'})
    @responds(schema=OpeningSchema)
    @ns.response(200, 'Success')
    @ns.response(404, 'Not found')
    def get(self, id: int) -> Opening:
        """Returns openings by id"""

        opening = OpeningService.get_by_id(id)
        if opening:
            return opening
        else:
            abort(404)
