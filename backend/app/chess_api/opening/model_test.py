from pytest import fixture
from .model import Opening


@fixture
def opening():
    return Opening(
        eco_code='test',
        name='test',
        variation='test',
        moves='test',
        final_position='test'
    )


def test_Opening_create(opening):
    assert opening
