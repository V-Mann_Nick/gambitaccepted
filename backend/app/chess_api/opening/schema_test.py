from pytest import fixture
from typing import Dict

from flask_sqlalchemy import SQLAlchemy

from app.test.fixtures import app, db
from app.test.fixtures_openings import sample_openings, db_with_openings  # noqa
from app.chess_api.game.model import Game
from app.chess_api.game.service import GameService
from .model import Opening
from .interface import OpeningInterface
from .schema import OpeningSchema, OpeningSchemaDetermine, OpeningSchemaQuery


@fixture
def schema() -> OpeningSchema:
    return OpeningSchema()


@fixture
def schemaDetermine() -> OpeningSchemaDetermine:
    return OpeningSchemaDetermine()

@fixture
def schemaQuery() -> OpeningSchemaQuery:
    return OpeningSchemaQuery()


@fixture
def pgn() -> str:
    return '\n'.join([
        '[Event "chess.com QF Blitz 3m+2spm 2016"]',
        '[Site "chess.com INT"]',
        '[Date "2016.06.23"]',
        '[Round "11"]',
        '[White "Magnus Carlsen"]',
        '[Black "Tigran L Petrosian"]',
        '[Result "1-0"]',
        '[ECO "B00"]',
        '[Opening "KP"]',
        '[Variation "Nimzovich defence"]',
        '[WhiteElo "2851"]',
        '[BlackElo "2611"]',
        '',
        '1. e4 Nc6 2. Nf3 d6 3. d4 Nf6 4. Nc3 Bg4 5. d5 Nb8 6. h3 Bxf3 7. Qxf3 Nbd7 8.',
        'g4 g6 9. Be3 Bg7 10. O-O-O c5 11. Qe2 b5 12. g5 Nh5 13. Nxb5 Rb8 14. c3 Qa5 15.',
        'a4 Nb6 16. Nc7+ Kd8 17. Qa6 Qxa4 18. Bb5 Qxa6 19. Nxa6 Rb7 20. Bc6 Kc8 21. b4',
        'Nc4 22. bxc5 Nxe3 23. fxe3 Rb3 24. Rd4 dxc5 25. Kc2 cxd4 26. Kxb3 dxe3 27. Kc2',
        'Be5 28. Rb1 e2 29. Kd2 Ng3 30. Rb7 Bxc3+ 31. Kd3 Be5 32. Rxa7 Bb8 33. Ra8 e1=Q',
        '34. Rxb8# 1-0'
    ])


@fixture
def game(db_with_openings: SQLAlchemy, pgn: str) -> Game:
    return GameService.create(dict(pgn=pgn))


def test_OpeningSchema_create(schema: OpeningSchema):
    assert schema


def test_OpeningSchema_serialize(schema: OpeningSchema, game: Game):
    opening: Opening = game.opening
    result: Dict = schema.dump(opening)

    assert result.get('ecoCode') == 'B00'
    assert result.get('name') == 'KP'
    assert result.get('variation') == 'Nimzovich defence'
    assert result.get('moves') == '1. e4 Nc6 *'
    assert result.get('finalPosition') == 'r1bqkbnr/pppppppp/2n5/8/4P3/8/PPPP1PPP/RNBQKBNR w KQkq - 1 2'
    assert {'id': game.id} in result.get('gameIds')


def test_OpeningSchemaDetermine_deserialize(schemaDetermine: OpeningSchemaDetermine, pgn: str):
    result: Dict = schemaDetermine.load({'pgn': pgn})

    assert result.get('pgn') == pgn


def test_OpeningSchemaQuery_deserialize(schemaQuery: OpeningSchemaQuery):
    result: Dict = schemaQuery.load({'eco': 'C24', 'name': 'anything'})

    assert result.get('eco_code') == 'C24'
    assert result.get('name') == 'anything'
