from typing import List
from io import StringIO

from flask_sqlalchemy import SQLAlchemy
from pytest import fixture
import chess
from chess.pgn import read_game

from app.test.fixtures import app, db
from app.test.fixtures_openings import sample_openings, db_with_openings
from .service import OpeningService
from .model import Opening
from .interface import OpeningInterface
from .schema import OpeningSchemaQuery


@fixture
def pgns():
    return [
        # Correct opening at index 4 of opening samples
        '\n'.join([
            '[Event "43. Olympiad 2018"]',
            '[Site "Batumi GEO"]',
            '[Date "2018.10.03"]',
            '[Round "9.18"]',
            '[Result "1/2-1/2"]',
            '[White "Dimitrios Mastrovasilis"]',
            '[Black "Nodirbek Yakubboev"]',
            '[ECO "C26"]',
            '',
            '1. e4 e5 2. Nc3 Nf6 3. d4 d6 4. dxe5 dxe5 5. Qxd8+ Kxd8 6. Bc4 Bb4 7. Bxf7 Rf8',
            '8. Bb3 Nxe4 9. Ne2 Bf5 10. Be3 Nd7 11. O-O-O Nxc3 12. Nxc3 c6 13. Ne2 Kc7 14.',
            'Ng3 Bg6 15. c3 Be7 16. Bc2 Bxc2 17. Kxc2 Nf6 18. Rhe1 Nd5 19. Ne4 Rad8 20. Rd3',
            'h6 21. Bd2 b6 22. a3 Bh4 23. c4 Nf6 24. Rxd8 Kxd8 25. g3 Nxe4 26. Rxe4 Be7 27.',
            'Be3 Bf6 28. a4 Kd7 29. a5 Bg5 30. Bxg5 hxg5 31. Rxe5 Rxf2+ 32. Kb3 Rxh2 33.',
            'Rxg5 Rg2 34. a6 b5 35. Rxg7+ Kd6 36. cxb5 cxb5 37. Kc3 Kc5 38. b4+ Kd5 39. Kd3',
            'Ra2 40. Rg5+ Ke6 41. Kd4 Kf6 42. Rxb5 Rxa6 43. Kc5 Re6 44. Rb7 Ra6 45. Rb5 Re6',
            '46. Rb7 Ra6 47. Rc7 Kf5 48. Kb5 Ra3 49. Kc4 Kg4 50. Rg7+ Kf5 51. Kb5 a6+ 52.',
            'Kc4 Ra1 53. Rd7 Kg4 54. Rd3 Ra4 55. Kb3 Ra1 56. Rc3 Kh3 57. Kb2 Ra4 58. Kb3 Ra1',
            '59. Kc4 Ra4 60. Kc5 Kg4 61. Rd3 Kh3 62. Rb3 Kg4 63. Rb1 Kxg3 64. Kc4 a5 65. Kb5',
            'Rxb4+ 66. Rxb4 axb4 67. Kxb4 1/2-1'

        ]),
        # Correct opening at index 2 of opening samples
        '\n'.join([
            '[Event "Mediterranean Flower"]',
            '[Site "Rijeka CRO"]',
            '[Date "2015.03.03"]',
            '[Round "3.4"]',
            '[Result "1/2-1/2"]',
            '[White "Caterina Leonardi"]',
            '[Black "Laura Unuk"]',
            '[ECO "A34"]',
            '',
            '1. c4 Nf6 2. Nc3 c5 3. g3 g6 4. Bg2 Bg7 5. Nf3 O-O 6. O-O d5 7. cxd5 Nxd5 8. d4',
            'cxd4 9. Nxd5 Qxd5 10. Be3 Qb5 11. Nxd4 Qxb2 12. Rb1 Qxa2 13. Ra1 Qb2 14. Rb1',
            'Qa2 15. Ra1 Qb2 16. Rb1 1/2-1/2'
        ]),

        # Correct opening not in opening samples
        '\n'.join([
            '[Event "World Blitz 2018"]',
            '[Site "St Petersburg RUS"]',
            '[Date "2018.12.30"]',
            '[Round "20.39"]',
            '[Result "1/2-1/2"]',
            '[White "Ivan RUS Popov"]',
            '[Black "Alexander Riazantsev"]',
            '[ECO "C15"]',
            '',
            '1. e4 e6 2. d4 d5 3. Nc3 Bb4 4. exd5 exd5 5. Bd3 Nf6 6. Ne2 O-O 7. O-O c6 8.',
            'Ng3 Re8 9. h3 Nbd7 10. Qf3 Nf8 11. Bg5 Be7 12. Rae1 Ne6 13. Be3 Bf8 14. Nf5 Bd7',
            '15. Qg3 Kh8 16. Nd6 Bxd6 17. Qxd6 Qb8 18. Qxb8 Raxb8 19. Ne2 Nf8 20. Bf4 Rbc8',
            '21. Ng3 Ng6 22. Bd6 Nh4 23. Be7 Bxh3 24. Bxf6 Rxe1 25. Bxh4 Rxf1+ 26. Kxf1 Be6',
            '27. Be7 Kg8 28. Bd6 g6 29. Ne2 f6 30. Nf4 Kf7 31. Nxe6 Kxe6 32. Bg3 Re8 33. f3',
            'a6 34. a4 Kf7 35. a5 h5 36. Kf2 Re6 37. Bc7 Re7 38. Bd6 Re6 39. Bb4 Re8 40. b3',
            'Rd8 41. Bc5 Rg8 42. c3 Ke6 43. Ba3 h4 44. Bc1 g5 45. g4 hxg3+ 46. Kxg3 Rh8 47.',
            'Kg2 Rh4 48. Be3 Rh8 49. Bf2 Rc8 50. b4 Rh8 51. Bg3 Rh6 52. Bf2 Rh8 53. Bf1 f5',
            '54. Bd3 Kf6 55. Bg3 f4 56. Bf2 Re8 57. Kf1 Rh8 58. Bg1 Re8 59. Bf2 Re7 60. Kg2',
            'Re8 61. Kh3 Rh8+ 62. Kg2 Re8 63. Kf1 Rh8 64. Kg1 Re8 65. Bf1 Re7 66. Bd3 Re8',
            '67. Kf1 Rh8 68. Kg2 Re8 69. Kf1 Re7 70. Kg2 Re8 71. Kf1 Rh8 72. Kg2 Rg8 73. Kh3',
            'Rh8+ 74. Kg2 Ke6 75. Kf1 Rh1+ 76. Bg1 Kf6 77. Kg2 Rh8 78. Bf2 Re8 79. Kf1 Rg8',
            '80. Be2 Kg6 81. Bd3+ Kf6 82. Be2 Re8 83. Bd1 Re7 84. Be2 Re8 85. Bd3 Re7 86.',
            'Kg1 Re8 87. Kg2 Re7 88. Kg1 Re8 1/2-1/2'
        ]),
        '\n'.join([
            '[Event "chess.com QF Blitz 3m+2spm 2016"]',
            '[Site "chess.com INT"]',
            '[Date "2016.06.23"]',
            '[Round "13"]',
            '[White "Magnus Carlsen"]',
            '[Black "Tigran L Petrosian"]',
            '[Result "1-0"]',
            '[ECO "B07"]',
            '[Opening "Pirc"]',
            '[Variation "Ufimtsev-Pytel variation"]',
            '[WhiteElo "2851"]',
            '[BlackElo "2611"]',
            '',
            '1. e4 d6 2. d4 Nf6 3. Nc3 c6 4. Nf3 g6 5. h3 Bg7 6. Be3 O-O 7. a4 Nbd7 8. Qd2',
            'e5 9. dxe5 dxe5 10. a5 Qe7 11. Bd3 Nc5 12. O-O Nh5 13. Rfd1 Ne6 14. Bf1 Nhf4',
            '15. Kh2 Rd8 16. Qe1 Nd4 17. Nxd4 exd4 18. Bxf4 dxc3 19. Rxd8+ Qxd8 20. bxc3 Be6',
            '21. e5 Qd5 22. Qe3 Re8 23. c4 Qd8 24. Qxa7 Bc8 25. Re1 g5 26. Bg3 h5 27. Kg1 f5',
            '28. h4 f4 29. Bh2 Rxe5 30. Rxe5 Bxe5 31. hxg5 Qxg5 32. Qa8 Qd8 33. a6 bxa6 34.',
            'Qxc6 Bf5 35. Qxa6 Qc7 36. Qh6 Qf7 37. Bxf4 Bg7 38. Qg5 Bxc2 39. Be2 Bg6 40. c5',
            'Bf6 41. Qg3 h4 42. Qf3 Kg7 43. c6 h3 44. c7 Qe6 45. Qxh3 Bf5 46. Qg3+ Kh7 47.',
            'Qe3 Qc6 48. Bd3 1-0'
        ]),
        '\n'.join([
            '[Event "chess.com QF Blitz 3m+2spm 2016"]',
            '[Site "chess.com INT"]',
            '[Date "2016.06.23"]',
            '[Round "11"]',
            '[White "Magnus Carlsen"]',
            '[Black "Tigran L Petrosian"]',
            '[Result "1-0"]',
            '[ECO "B00"]',
            '[Opening "KP"]',
            '[Variation "Nimzovich defence"]',
            '[WhiteElo "2851"]',
            '[BlackElo "2611"]',
            '',
            '1. e4 Nc6 2. Nf3 d6 3. d4 Nf6 4. Nc3 Bg4 5. d5 Nb8 6. h3 Bxf3 7. Qxf3 Nbd7 8.',
            'g4 g6 9. Be3 Bg7 10. O-O-O c5 11. Qe2 b5 12. g5 Nh5 13. Nxb5 Rb8 14. c3 Qa5 15.',
            'a4 Nb6 16. Nc7+ Kd8 17. Qa6 Qxa4 18. Bb5 Qxa6 19. Nxa6 Rb7 20. Bc6 Kc8 21. b4',
            'Nc4 22. bxc5 Nxe3 23. fxe3 Rb3 24. Rd4 dxc5 25. Kc2 cxd4 26. Kxb3 dxe3 27. Kc2',
            'Be5 28. Rb1 e2 29. Kd2 Ng3 30. Rb7 Bxc3+ 31. Kd3 Be5 32. Rxa7 Bb8 33. Ra8 e1=Q',
            '34. Rxb8# 1-0'
        ]),
        '\n'.join([
            '[Event "chess.com QF Blitz 1m+1spm 2016"]',
            '[Site "chess.com INT"]',
            '[Date "2016.06.23"]',
            '[Round "18"]',
            '[White "Tigran L Petrosian"]',
            '[Black "Magnus Carlsen"]',
            '[Result "1-0"]',
            '[ECO "B06"]',
            '[Opening "Robatsch (modern) defence"]',
            '[Variation "None"]',
            '[WhiteElo "2851"]',
            '[BlackElo "2611"]',
            '',
            '1. e4 g6 2. d4 Bg7 3. Nf3 d6 4. Bc4 Nf6 5. Qe2 O-O 6. O-O c6 7. Bb3 a5 8. a4 e6',
            '9. e5 Nd5 10. exd6 Qxd6 11. Na3 Qc7 12. Re1 Nd7 13. Qe4 h6 14. c3 N7f6 15. Qh4',
            'g5 16. Bxg5 hxg5 17. Nxg5 Qf4 18. Qxf4 Nxf4 19. Rad1 N6d5 20. Nc4 Rd8 21. Ne4',
            'b5 22. Ne5 Bxe5 23. dxe5 Kg7 24. g3 Ng6 25. f4 Rb8 26. Nd6 bxa4 27. Bxa4 Rxb2',
            '28. Bxc6 Ba6 29. f5 Nxc3 30. Rd4 Ne2+ 31. Rxe2 Rxe2 32. fxg6 Kxg6 33. Rg4+ Kh7',
            '34. Be4+ Rxe4 35. Nxe4 Rd1+ 36. Kf2 Rf1+ 37. Kg2 Rf5 38. Nf6+ Kh6 39. Rg8 Bf1+',
            '40. Kg1 Be2 41. h4 a4 42. g4 Bxg4 43. Rxg4 Rxe5 44. Rxa4 Rf5 45. Ng4+ Kh5 46.',
            'Ne3 Rf3 47. Ng2 Rg3 48. Kf2 Rg4 49. Ra7 f5 50. Re7 Re4 51. Kg3 e5 52. Rg7 f4+',
            '53. Kf3 Kh6 54. Rg5 Ra4 55. Rxe5 Ra3+ 56. Kxf4 Ra4+ 57. Kg3 Ra3+ 58. Kg4 Ra8',
            '59. Nf4 Rg8+ 60. Rg5 Ra8 61. Rg6+ 1-0',
        ])
    ]


@fixture
def parsed_pgns(pgns):
    return [read_game(StringIO(pgn)) for pgn in pgns]


def test_get_all(db: SQLAlchemy):
    opening1: Opening = Opening(
        eco_code='test1',
        name='test1',
        variation='test1',
        final_position='test1',
    )
    opening2: Opening = Opening(
        eco_code='test2',
        name='test2',
        variation='test2',
        final_position='test2',
    )
    db.session.add(opening1)
    db.session.add(opening2)
    db.session.commit()

    results: List[Opening] = OpeningService.get_all()

    assert len(results) == 2
    assert opening1 in results and opening2 in results


def test_query(db_with_openings: SQLAlchemy, sample_openings: List[Opening]):
    result: List[Opening] = OpeningService.query(OpeningSchemaQuery().load({
        'eco': 'C26',
        'name': 'Vienna game'
    }))
    assert sample_openings[5] in result


def test_update(db: SQLAlchemy):
    opening: Opening = Opening(
        eco_code='test',
        name='test',
        variation='test',
        final_position='test',
    )
    db.session.add(opening)
    db.session.commit()

    updates: OpeningInterface = dict(name='test changes')
    OpeningService.update(opening, updates)

    result: Opening = Opening.query.get(opening.id)
    assert result.name == 'test changes'


def test_best_match(
    db: SQLAlchemy,
    parsed_pgns: List[chess.pgn.Game],
    sample_openings: List[Opening],
):
    db.session.add_all(sample_openings)
    db.session.commit()

    result: Union[Opening, None] = OpeningService.best_match(parsed_pgns[0], Opening.query.all())

    assert result is sample_openings[4]


def test_determine(
    db: SQLAlchemy,
    parsed_pgns: List[chess.pgn.Game],
    sample_openings: List[Opening],
):
    db.session.add_all(sample_openings)
    db.session.commit()

    # case ambigious
    game: chess.pgn.Game = parsed_pgns[0]
    result: Union[Opening, None] = OpeningService.determine(game)
    assert result is sample_openings[4]


    # case definite
    game: chess.pgn.Game = parsed_pgns[1]
    result: Union[Opening, None] = OpeningService.determine(game)
    assert result is sample_openings[2]

    # case not found
    game: chess.pgn.Game = parsed_pgns[2]
    result: Union[Opening, None] = OpeningService.determine(game)
    assert result is None
