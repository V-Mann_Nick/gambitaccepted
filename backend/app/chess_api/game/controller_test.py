from unittest.mock import patch
from typing import List

from flask.testing import FlaskClient

from app.test.fixtures import app, client  # noqa
from .model import Game
from .schema import GameSchema
from .service import GameService
