from pytest import fixture
from .model import Game


@fixture
def game():
    return Game(
        event='test',
        site='test',
        date='test',
        match_round='test',
        result='test',
        white_elo=2500,
        black_elo=2500,
        annotater='test',
        move_text='test'
    )


def test_Game_create(game):
    assert game
