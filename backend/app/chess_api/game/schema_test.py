from pytest import fixture
from typing import Dict

from flask_sqlalchemy import SQLAlchemy
from marshmallow import ValidationError

from app.test.fixtures import app, db
from app.test.fixtures_openings import sample_openings, db_with_openings
from .model import Game
from .service import GameService
from .interface import GameInterfaceNew
from .schema import GameSchema, GameSchemaPgn, GameSchemaList, GameSchemaQuery


@fixture
def schema() -> GameSchema:
    return GameSchema()


@fixture
def schemaPgn() -> GameSchemaPgn:
    return GameSchemaPgn()


@fixture
def schemaList() -> GameSchemaList:
    return GameSchemaList()


@fixture
def schemaQuery() -> GameSchemaQuery:
    return GameSchemaQuery()


@fixture
def pgn() -> str:
    return '\n'.join([
        '[Event "43. Olympiad 2018"]',
        '[Site "Batumi GEO"]',
        '[Date "2018.10.03"]',
        '[Round "9.18"]',
        '[Result "1/2-1/2"]',
        '[White "Dimitrios Mastrovasilis"]',
        '[Black "Nodirbek Yakubboev"]',
        '[ECO "C26"]',
        '',
        '1. e4 e5 2. Nc3 Nf6 3. d4 d6 4. dxe5 dxe5 5. Qxd8+ Kxd8 6. Bc4 Bb4 7. Bxf7 Rf8',
        '8. Bb3 Nxe4 9. Ne2 Bf5 10. Be3 Nd7 11. O-O-O Nxc3 12. Nxc3 c6 13. Ne2 Kc7 14.',
        'Ng3 Bg6 15. c3 Be7 16. Bc2 Bxc2 17. Kxc2 Nf6 18. Rhe1 Nd5 19. Ne4 Rad8 20. Rd3',
        'h6 21. Bd2 b6 22. a3 Bh4 23. c4 Nf6 24. Rxd8 Kxd8 25. g3 Nxe4 26. Rxe4 Be7 27.',
        'Be3 Bf6 28. a4 Kd7 29. a5 Bg5 30. Bxg5 hxg5 31. Rxe5 Rxf2+ 32. Kb3 Rxh2 33.',
        'Rxg5 Rg2 34. a6 b5 35. Rxg7+ Kd6 36. cxb5 cxb5 37. Kc3 Kc5 38. b4+ Kd5 39. Kd3',
        'Ra2 40. Rg5+ Ke6 41. Kd4 Kf6 42. Rxb5 Rxa6 43. Kc5 Re6 44. Rb7 Ra6 45. Rb5 Re6',
        '46. Rb7 Ra6 47. Rc7 Kf5 48. Kb5 Ra3 49. Kc4 Kg4 50. Rg7+ Kf5 51. Kb5 a6+ 52.',
        'Kc4 Ra1 53. Rd7 Kg4 54. Rd3 Ra4 55. Kb3 Ra1 56. Rc3 Kh3 57. Kb2 Ra4 58. Kb3 Ra1',
        '59. Kc4 Ra4 60. Kc5 Kg4 61. Rd3 Kh3 62. Rb3 Kg4 63. Rb1 Kxg3 64. Kc4 a5 65. Kb5',
        'Rxb4+ 66. Rxb4 axb4 67. Kxb4 1/2-1'
    ])


@fixture
def game(pgn: str, db_with_openings: SQLAlchemy) -> Game:
    return GameService.create(dict(pgn=pgn))


def test_GameSchema_create(schema: GameSchema):
    assert schema


def test_GameSchemaPgn_create(schemaPgn: GameSchemaPgn):
    assert schemaPgn


def test_GameSchemaList_create(schemaList: GameSchemaList):
    assert schemaList


def test_GameSchemaQuery_create(schemaQuery: GameSchemaQuery):
    assert schemaQuery


def test_GameSchema_serialize(schema: GameSchema, game: Game):
    result: Dict = schema.dump(game)

    assert result.get('event') == '43. Olympiad 2018'
    assert result.get('site') == 'Batumi GEO'
    assert result.get('date') == '2018.10.03'
    assert result.get('matchRound') == '9.18'
    assert result.get('result') == '1/2-1/2'
    assert result.get('players')[0]
    assert result.get('players')[1]
    assert result.get('moveText')


def test_GameSchemaPgn_deserialize(schemaPgn: GameSchemaPgn, pgn: str):
    params: GameInterfaceNew = schemaPgn.load(dict(pgn=pgn))
    assert params.get('pgn') == pgn


def test_GameSchemaList_serialize(schemaList: GameSchemaList, game: Game):
    result: Dict = schemaList.dump({
        'foundGames': 1,
        'pageSize': 10,
        'pageNumber': 1,
        'pageTotal': 1,
        'games': [game]
    })

    assert result.get('foundGames') == 1
    assert result.get('pageSize') == 10
    assert result.get('pageNumber') == 1
    assert result.get('pageTotal') == 1
    assert result.get('games')


def test_GameSchemaQuery_deserialize(schemaQuery: GameSchemaQuery):
    # test pageSize pageNumber required
    query = {}
    try:
        result = schemaQuery.load(query)
        assert not result
    except ValidationError as e:
        assert e

    # valid case
    query = {
        'pageSize': 10,
        'pageNumber': 1,
        'player1': 'A player',
        'player1Color': 'w',
        'player2': 'An other player',
        'player2Color': 'b',
        'event': 'any',
        'site': 'any',
        'whiteElo': '>2000',
        'blackElo': '<3000',
        'result': '1-0'
    }
    try:
        result = schemaQuery.load(query)
    except ValidationError as e:
        assert not e
    assert result.get('page_size') == 10
    assert result.get('page_number') == 1
    assert result.get('player1') == 'A player'
    assert result.get('player1_color') == 'w'
    assert result.get('player2') == 'An other player'
    assert result.get('player2_color') == 'b'
    assert result.get('event') == 'any'
    assert result.get('site') == 'any'
    assert result.get('white_elo') == '>2000'
    assert result.get('black_elo') == '<3000'
    assert result.get('result') == '1-0'

    # everything wrong
    query = {
        'pageSize': 21,
        'pageNumber': 0,
        'player1': 'Charles,',
        'player1Color': 'wh',
        'player2': 'Blake Bluffer Bluffingtion !',
        'player2Color': 'bl',
        'whiteElo': '!>2000',
        'blackElo': '!<3000',
        'result': '1-00'
    }
    try:
        result = schemaQuery.load(query)
        assert not result
    except ValidationError as e:
        assert len(e.args[0]) == 9

    # twice same color
    query = {
        'pageSize': 10,
        'pageNumber': 1,
        'player1': 'p1',
        'player1Color': 'w',
        'player2': 'p2',
        'player2Color': 'w',
    }
    try:
        result = schemaQuery.load(query)
        assert not result
    except ValidationError as e:
        assert e

    # color no player
    query = {
        'pageSize': 10,
        'pageNumber': 1,
        'player1Color': 'w',
    }
    try:
        result = schemaQuery.load(query)
        assert not result
    except ValidationError as e:
        assert e


