from datetime import datetime

from sqlalchemy import Integer, Column, String, Text, DateTime
from sqlalchemy.orm import relationship
from sqlalchemy.schema import ForeignKey

from app import db
from .interface import GameInterfaceUpdate


class Game(db.Model):
    id = Column(Integer, primary_key=True)
    event = Column(String(64), index=True)
    site = Column(String(64), index=True)
    date = Column(String(16), index=True)
    match_round = Column(String(32))
    result = Column(String(16), index=True)
    players = relationship('Player', secondary='pairing', back_populates='games', lazy='dynamic')
    white_elo = Column(Integer, index=True)
    black_elo = Column(Integer, index=True)
    opening_id = Column(Integer, ForeignKey('opening.id'))
    annotater = Column(String(32), index=True)
    move_text = Column(Text(), index=True)
    timestamp = Column(DateTime, default=datetime.utcnow)

    def __repr__(self):
        return f'<{self.players[0].full_name()} - {self.players[1].full_name()} ({self.date})>'

    def update(self, changes: GameInterfaceUpdate):
        for key, val in changes.items():
            setattr(self, key, val)
        return self
