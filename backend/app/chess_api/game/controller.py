from typing import List, Set, Dict, Tuple, Optional

from flask import request, Response
from flask_restplus import Namespace, Resource, abort
from flask_accepts import responds, accepts
from marshmallow import ValidationError
from flask_sqlalchemy import Pagination

from .service import GameService
from .model import Game
from .schema import GameSchema, GameSchemaList, GameSchemaQuery


ns = Namespace('Game')


@ns.route('/')
class GameResource(Resource):
    @accepts(
        dict(name='pageSize', type=int, default=10),
        dict(name='pageNumber', type=int, default=1),
        dict(name='player1', type=str),
        dict(name='player1Color', type=str),
        dict(name='player2', type=str),
        dict(name='player2Color', type=str),
        dict(name='event', type=str),
        dict(name='site', type=str),
        dict(name='whiteElo', type=str),
        dict(name='blackElo', type=str),
        dict(name='result', type=str),
        api=ns
    )
    @responds(schema=GameSchemaList)
    @ns.doc(
        params={
            'pageSize': 'games per page (1-20)',
            'pageNumber': 'current page number',
            'player1': 'one of two players',
            'player1Color': 'color of first player ("w", "white", "b", "black")',
            'player2': 'one of two players',
            'player2Color': 'color of second player ("w", "white", "b", "black")',
            'event': 'tournament or event of some sorts',
            'site': 'location or website',
            'whiteElo': 'lower or greater than some number (e.g. ">2000", "<3000")',
            'blackElo': 'lower or greater than some number (e.g. ">2000", "<3000")',
            'result': 'result of game ("1-0", "0-1" or "1/2-1/2")'
        }
    )
    @ns.response(200, 'Success')
    @ns.response(400, 'Bad Request')
    def get(self) -> Dict:
        """Returns games filtered by query or all of them."""

        # validate parameters
        try:
            args_none_removed: Dict = {
                k: v for k, v in request.parsed_args.items() if v is not None
            }
            query_params: Dict = GameSchemaQuery().load(args_none_removed)
        except ValidationError as e:
            abort(400, **e.messages)

        result: Pagination = GameService.query(query_params)

        return {
            'foundGames': result.total,
            'pageSize': result.per_page,
            'pageNumber': result.page,
            'pageTotal': result.pages,
            'games': result.items
        }


@ns.route('/<int:id>')
class GameIdResource(Resource):
    @responds(schema=GameSchema)
    @ns.response(200, 'Success')
    @ns.response(404, 'Not found')
    def get(self, id) -> Game:
        """Returns game"""

        game = GameService.get_by_id(id)
        if game:
            return game
        else:
            abort(404)


@ns.route('/<int:id>/pgn')
class GamePgnResource(Resource):
    @ns.response(200, 'Success')
    @ns.response(404, 'Not found')
    def get(self, id) -> Dict:
        """Returns pgn"""

        game = GameService.get_pgn(id)
        if game:
            return Response(game, mimetype='text/pgn')
        else:
            abort(404)
