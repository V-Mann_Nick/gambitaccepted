from typing import List, Union, Tuple, Optional, Dict
from io import StringIO
import os

from flask_sqlalchemy import BaseQuery, Pagination
from sqlalchemy.orm import aliased
from sqlalchemy.orm.util import AliasedClass
import chess

from app import db
from app.chess_api.player.model import Player, Pairing
from app.chess_api.opening.service import OpeningService
from app.chess_api.player.service import PlayerService
from .model import Game
from .interface import GameInterfaceUpdate, GameInterfaceNew


class GameService:
    @staticmethod
    def get_by_id(id: int) -> Game:
        """Returns game for givn id"""

        return Game.query.get(id)


    @staticmethod
    def get_pgn(id: int) -> str:
        """Returns pgn of given id"""

        game: Game = GameService.get_by_id(id)
        pgn: str =  f'[Event "{game.event}"]\n'
        pgn += f'[Site "{game.site}"]\n'
        pgn += f'[Date "{game.date}"]\n'
        pgn += f'[Round "{game.match_round}"]\n'
        pgn += f'[White "{game.players[0].full_name()}"]\n'
        pgn += f'[Black "{game.players[1].full_name()}"]\n'
        pgn += f'[Result "{game.result}"]\n'
        if game.opening:
            pgn += f'[ECO "{game.opening.eco_code}"]\n'
            pgn += f'[Opening "{game.opening.name}"]\n'
            if game.opening.variation:
                pgn += f'[Variation "{game.opening.variation}"]\n'
        if game.white_elo:
            pgn += f'[WhiteElo "{game.white_elo}"]\n'
        if game.black_elo:
            pgn += f'[BlackElo "{game.black_elo}"]\n'
        if game.annotater:
            pgn += f'[Annotator "{game.annotater}"]\n'
        pgn += '\n'
        pgn += game.move_text
        return pgn

    @staticmethod
    def query(params: Dict, context: Optional[Union[BaseQuery, None]]=None) -> Pagination:
        """Queries database with given params. params is loaded via GameSchemaQuery.
        params:
            'pageSize': int
            'pageNumber': int
            'player1': str
            'player1Color': str
            'player2': str
            'player2Color': str
            'event': str
            'site': str
            'whiteElo': str
            'blackElo': str
            'result': str
        }
        context: BaseQuery object which sets context for further querying
        """
        # remove from params for further usage later
        page_size = params.pop('page_size', None)
        page_number = params.pop('page_number', None)

        # edit player params
        player1_query: Union[Tuple[str], None] = (
            params.pop('player1', None),
            params.pop('player1_color', None)
        ) if params.get('player1') else None
        player2_query: Union[Tuple[str], None] = (
            params.pop('player2', None),
            params.pop('player2_color', None)
        ) if params.get('player2') else None

        # create Query object or set context
        query: BaseQuery = Game.query if not context else context

        # add player queries to query
        for player_query in (player1_query, player2_query):
            if player_query:
                name, color = player_query
                pairing_alias: AliasedClass = aliased(Pairing)
                player_alias: AliasedClass = aliased(Player)
                query = query\
                    .join(pairing_alias, Game.id == pairing_alias.game_id)\
                    .join(player_alias, pairing_alias.player_id == player_alias.id)
                if len(name.strip().split(' ')) > 1:  # at least first name and last name
                    first_name, middle_names, last_name = PlayerService.split_name(name)
                    query = query.filter(
                        player_alias.first_name.contains(first_name),  # and
                        player_alias.last_name.contains(last_name)
                    )
                    if middle_names:
                        query = query.filter(player_alias.middle_names.contains(middle_names))
                else:  # only one name. Can be either of the three
                    query = query.filter(
                        player_alias.first_name.contains(name) |  # or
                        player_alias.middle_names.contains(name) |  # or
                        player_alias.last_name.contains(name)
                    )
                if color:
                    query = query.filter(pairing_alias.color == color)

        # add rest of the queries to the query
        for key, value in params.items():
            if any([key == k for k in ('event', 'site', 'result')]):
                query = query.filter(getattr(Game, key).contains(value))
            elif any([key == k for k in ('white_elo', 'black_elo')]):
                if value[0] == '<':
                    query = query.filter(getattr(Game, key) < int(value[1:]))
                else:
                    query = query.filter(getattr(Game, key) > int(value[1:]))

        return query.paginate(page=page_number, per_page=page_size)


    @staticmethod
    def update(game: Game, game_change_update: GameInterfaceUpdate) -> Game:
        """update a Game via GameInterfaceUpdate"""

        game.update(game_change_update)
        db.session.commit()
        return game


    @staticmethod
    def delete(game: Game):
        """not yet implemented"""


    @staticmethod
    def create(game: chess.pgn.Game) -> Game:
        """creates game from chess.pgn.Game Object
        and adds neccessary player objects to db"""

        # Errors?
        if game.errors:
            print(game.errors)
            return

        # Find entries in db or create them
        opening: Union[Opening, None] = OpeningService.determine(game)
        white_player: Player = PlayerService.determine(game.headers.get('White'), create=True)
        black_player: Player = PlayerService.determine(game.headers.get('Black'), create=True)
        if white_player is black_player:
            print('\n' + white_player.full_name() + 'is playing against himself.')
            print('Skipping....\n\n')
            return

        # Create dictionary for creating game entry
        game_interface: GameInterfaceNew = GameInterfaceNew(
            event=game.headers.get('Event'),
            site=game.headers.get('Site'),
            date=game.headers.get('Date'),
            match_round=game.headers.get('Round'),
            result=game.headers.get('Result'),
            annotater=game.headers.get('Annotator'),
            move_text=game.accept(
                chess.pgn.StringExporter(
                    headers=False,
                    variations=True,
                    comments=True
                )
            )
        )
        try:
            white_elo = int(game.headers.get('WhiteElo'))
            game_interface['white_elo'] = white_elo
        except:
            pass
        try:
            black_elo = int(game.headers.get('WhiteElo'))
            game_interface['black_elo'] = black_elo
        except:
            pass
        if opening:
            game_interface['opening_id'] = opening.id

        # Create Game Object
        game = Game(**game_interface)

        # commit game to db
        db.session.add(game)
        db.session.commit()

        # Create Pairings and commit them
        pairing_white: Pairing = Pairing(player_id=white_player.id, game_id=game.id, color='w')
        db.session.add(pairing_white)
        db.session.commit()
        pairing_black: Pairing = Pairing(player_id=black_player.id, game_id=game.id, color='b')
        db.session.add(pairing_black)
        db.session.commit()

        print(f'{game.id} - {white_player.full_name()} vs {black_player.full_name()} ({game.date})')
        return game

    @staticmethod
    def create_from_file(path: str, encoding: str=None) -> List[Game]:
        """Create game objects from a .pgn file"""

        assert '.pgn' in path

        games: List[Game] = []
        with open(path, 'r', encoding=encoding) as f:
            while True:
                game: chess.pgn.Game = chess.pgn.read_game(f)
                # read_game returns None if file is at an end
                if not game:
                    break
                games.append(GameService.create(game))
        return games

    @staticmethod
    def create_from_folder(path: str) -> List[Game]:
        """Create game object from a directory. It selects all files,
        which have .pgn at the end for processing"""

        games: List[Game] = []
        pgn_files: List[str] = []
        for root, dirs, files in os.walk(path):
            for file in files:
                if file.endswith('.pgn'):
                    pgn_files.append(os.path.join(root, file))
        for pgn_file in pgn_files:
            games += GameService.create_from_file(pgn_file)
        return games
