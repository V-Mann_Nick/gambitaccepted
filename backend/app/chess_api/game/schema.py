from typing import Union

from marshmallow import Schema, fields, validate, ValidationError, validates_schema

from app.chess_api.player.schema import PlayerSchema
from app.chess_api.opening.schema import OpeningSchema


class GameSchema(Schema):
    id = fields.Integer(dump_only=True)
    event = fields.String(dump_only=True)
    site = fields.String(dump_only=True)
    date = fields.String(dump_only=True)
    matchRound = fields.String(attribute='match_round', dump_only=True)
    result = fields.String(dump_only=True)
    players = fields.Nested(PlayerSchema, exclude=('gameIds',), many=True, dump_only=True)
    whiteElo = fields.Integer(attribute='white_elo', dump_only=True)
    blackElo = fields.Integer(attribute='black_elo', dump_only=True)
    opening = fields.Nested(OpeningSchema, exclude=('gameIds',), dump_only=True)
    annotator = fields.String(attribute='annotater', dump_only=True)
    moveText = fields.String(attribute='move_text', dump_only=True)

    class Meta:
        ordered = True


class GameSchemaList(Schema):
    foundGames = fields.Integer()
    pageSize = fields.Integer()
    pageNumber = fields.Integer()
    pageTotal = fields.Integer()
    games = fields.Nested(GameSchema, exclude=('moveText',), many=True)

    class Meta:
        ordered = True


class GameSchemaQuery(Schema):
    """This schema is used for validation of given query arguments."""

    pageSize = fields.Integer(
        attribute='page_size',
        required=True,
        validate=validate.Range(min=1, max=20)
    )
    pageNumber = fields.Integer(
        attribute='page_number',
        required=True,
        validate=validate.Range(min=1)
    )
    # Match examples: 'Frink, Frank Middleman', 'Frank Frink', 'Frank'
    player1 = fields.String(validate=[
        validate.Regexp('^\w*(, | |)(?=((\w ?)*))\\2$'),
        validate.Length(max=64)
    ])
    player1Color = fields.String(
        attribute='player1_color',
        validate=validate.OneOf(['w', 'white', 'b', 'black'])
    )
    player2 = fields.String(validate=[
        validate.Regexp('^\w*(, | |)(?=((\w ?)*))\\2$'),
        validate.Length(max=64)
    ])
    player2Color = fields.String(
        attribute='player2_color',
        validate=validate.OneOf(['w', 'white', 'b', 'black'])
    )
    event = fields.String(validate=validate.Length(max=64))
    site = fields.String(validate=validate.Length(max=64))
    # Match examples: '<2000', '>2500'
    # No match: '<20000'
    whiteElo = fields.String(
        attribute='white_elo',
        validate=validate.Regexp('^(<|>)\d{1,4}$')
    )
    blackElo = fields.String(
        attribute='black_elo',
        validate=validate.Regexp('^(<|>)\d{1,4}$')
    )
    # Matches: '1/2-1/2', '1-0', '0-1'
    result = fields.String(validate=validate.Regexp('^(1\/2|1|0)-(1\/2|1|0)$'))

    @validates_schema
    def validate_player_colors(self, data, **kwargs):
        """schema level validation"""

        errors = dict()
        p1_color: Union[str, None] = data.get('player1_color')
        p2_color: Union[str, None] = data.get('player2_color')
        # player1Color and player2Color must be different
        if (
            p1_color and p2_color and (
                ('w' in p1_color and 'w' in p2_color) or
                ('b' in p1_color and 'b' in p2_color)
        )):
            errors['player1Color'] = [f'{p1_color} and {p2_color} describe the same color. Must be different!']
            errors['player2Color'] = [f'{p1_color} and {p2_color} describe the same color. Must be different!']

        p1: Union[str, None] = data.get('player1')
        p2: Union[str, None] = data.get('player2')
        # playerColor needs player to be defined
        if p1_color and not p1:
            errors['player1Color'] = [f'{p1_color} was given but no player name']
        if p2_color and not p2:
            errors['player2Color'] = [f'{p2_color} was given but no player name']

        if errors:
            raise ValidationError(errors)
