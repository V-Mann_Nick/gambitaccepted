from typing import TypedDict


class GameInterfaceUpdate(TypedDict):
    event: str
    site: str
    date: str
    match_round: str
    result: str
    white_elo: int
    black_elo: int
    opening_id: int
    annotater: str
    move_text: str


class GameInterfaceNew(TypedDict):
    event: str
    site: str
    date: str
    match_round: str
    result: str
    white_elo: int
    black_elo: int
    opening_id: int
    annotater: str
    move_text: str
