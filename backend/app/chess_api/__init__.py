def register_routes(api, app):
    from .game_printer.controller import ns as game_printer_api
    from .game.controller import ns as games_api
    from .player.controller import ns as players_api
    from .opening.controller import ns as openings_api

    api.add_namespace(game_printer_api, path=f'/gamePrinter')
    api.add_namespace(games_api, path=f'/game')
    api.add_namespace(players_api, path=f'/player')
    api.add_namespace(openings_api, path=f'/opening')
