import pytest
from typing import List

from flask_sqlalchemy import SQLAlchemy

from app.chess_api.opening.model import Opening
from .fixtures import app, db


@pytest.fixture
def sample_openings() -> List[Opening]:
    openings = list()
    openings.append(Opening(
        eco_code='E92',
        name="King's Indian",
        variation="Andersson variation",
        moves="1. d4 Nf6 2. c4 g6 3. Nc3 Bg7 4. e4 d6 5. Nf3 O-O 6. Be2 e5 7. dxe5 *",
        final_position='rnbq1rk1/ppp2pbp/3p1np1/4P3/2P1P3/2N2N2/PP2BPPP/R1BQK2R b KQ - 0 7'
    ))
    openings.append(Opening(
        eco_code='A51',
        name="Budapest defence declined",
        variation="None",
        moves="1. d4 Nf6 2. c4 e5 *",
        final_position='rnbqkb1r/pppp1ppp/5n2/4p3/2PP4/8/PP2PPPP/RNBQKBNR w KQkq - 0 3'
    ))
    openings.append(Opening(
        eco_code='A34',
        name="English",
        variation="symmetrical variation",
        moves="1. c4 c5 2. Nc3 Nf6 3. g3 *",
        final_position='rnbqkb1r/pp1ppppp/5n2/2p5/2P5/2N3P1/PP1PPP1P/R1BQKBNR b KQkq - 0 3'
    ))
    openings.append(Opening(
        eco_code='D20',
        name="QGA",
        variation="Linares variation",
        moves="1. d4 d5 2. c4 dxc4 3. e4 c5 4. d5 Nf6 5. Nc3 b5 *",
        final_position='rnbqkb1r/p3pppp/5n2/1ppP4/2p1P3/2N5/PP3PPP/R1BQKBNR w KQkq - 0 6'
    ))
    openings.append(Opening(
        eco_code='C26',
        name="Vienna",
        variation="Falkbeer variation",
        moves="1. e4 e5 2. Nc3 Nf6 *",
        final_position='rnbqkb1r/pppp1ppp/5n2/4p3/4P3/2N5/PPPP1PPP/R1BQKBNR w KQkq - 2 3'
    ))
    openings.append(Opening(
        eco_code='C26',
        name="Vienna game",
        moves="1. e4 e5 2. Nc3 Nf6 3. Bc4 *",
        final_position='rnbqkb1r/pppp1ppp/5n2/4p3/2B1P3/2N5/PPPP1PPP/R1BQK1NR b KQkq - 3 3'
    ))
    openings.append(Opening(
        eco_code='B07',
        name="Pirc",
        moves="1. e4 d6 2. d4 Nf6 3. Nc3 c6 *",
        final_position='rnbqkb1r/pp2pppp/2pp1n2/8/3PP3/2N5/PPP2PPP/R1BQKBNR w KQkq - 0 4'
    ))
    openings.append(Opening(
        eco_code='B00',
        name="KP",
        variation='Nimzovich defence',
        moves="1. e4 Nc6 *",
        final_position='r1bqkbnr/pppppppp/2n5/8/4P3/8/PPPP1PPP/RNBQKBNR w KQkq - 1 2'
    ))
    openings.append(Opening(
        eco_code='B06',
        name="Robatsch (modern) defence",
        moves="1. e4 g6 *",
        final_position='rnbqkbnr/pppppp1p/6p1/8/4P3/8/PPPP1PPP/RNBQKBNR w KQkq - 0 2'
    ))
    return openings


@pytest.fixture
def db_with_openings(db: SQLAlchemy, sample_openings: List[Opening]):
    db.session.add_all(sample_openings)
    db.session.commit()

    return db
