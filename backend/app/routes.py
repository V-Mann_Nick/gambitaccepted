def register_routes(api, app, root='api'):
    from app.chess_api import register_routes as attach_chess

    attach_chess(api, app)
