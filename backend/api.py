import os
import json

from app import create_app, db
from app.chess_api.game.model import Game
from app.chess_api.player.model import Player, Pairing
from app.chess_api.opening.model import Opening
from app.chess_api.game.service import GameService
from app.chess_api.player.service import PlayerService
from app.chess_api.opening.service import OpeningService

try:
    with open('/etc/config-ga.json') as f:
        env = json.load(f)['FLASK_ENV']
except BaseException:
    env = os.getenv("FLASK_ENV") or 'dev'

print(f"Active environment: * {env} *")
app = create_app(env)


@app.shell_context_processor
def make_shell_context():
    return {'db': db,
            'Game': Game,
            'Opening': Opening,
            'Player': Player,
            'Pairing': Pairing,
            'GameService': GameService,
            'PlayerService': PlayerService,
            'OpeningService': OpeningService}


if __name__ == '__main__':
    app.run(debug=True)
