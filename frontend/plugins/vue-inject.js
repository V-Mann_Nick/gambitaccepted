import Vue from 'vue'

Vue.prototype.$hashPgn = (pgn, stringLength = 10) => {
  const l = Math.ceil(pgn.length / stringLength)
  const sums = []
  for (let i = 0; i < stringLength; i++) {
    const part = pgn.slice(
      i * l,
      (i + 1) * l < pgn.length ? (i + 1) * l : pgn.length
    )
    sums.push(part.split('').reduce((sum, val) => sum + val.charCodeAt(0), 0))
  }
  const min = Math.min(...sums)
  const max = Math.max(...sums)
  const mapToAlphabet = (x) =>
    String.fromCharCode(
      Math.round(((122 - 97) * x + (97 * max - min * 122)) / (max - min))
    )
  sums.forEach((val, idx) => (sums[idx] = mapToAlphabet(val)))
  return sums.join('')
}

Vue.prototype.$removeFromCache = function(keepAlive = {}, key = undefined) {
  let cache, keys
  if (keepAlive && key) {
    cache = keepAlive.cache
    keys = keepAlive.keys
  } else {
    key = this.$vnode.key
    cache = this.$vnode.parent.componentInstance.cache
    keys = this.$vnode.parent.componentInstance.keys
  }

  delete cache[key]
  for (let i = 0; i < keys.length; i++) if (keys[i] === key) keys.splice(i, 1)
  console.log(cache, keys)
}
