import { cloneDeep } from 'lodash'

const startPosition = {
  brq: {
    coord: 'a8',
    isCaptured: false
  },
  bnq: {
    coord: 'b8',
    isCaptured: false
  },
  bbq: {
    coord: 'c8',
    isCaptured: false
  },
  bq: {
    coord: 'd8',
    isCaptured: false
  },
  bk: {
    coord: 'e8',
    isCaptured: false
  },
  bbk: {
    coord: 'f8',
    isCaptured: false
  },
  bnk: {
    coord: 'g8',
    isCaptured: false
  },
  brk: {
    coord: 'h8',
    isCaptured: false
  },
  bpa: {
    coord: 'a7',
    isCaptured: false,
    changePieceTypeTo: null
  },
  bpb: {
    coord: 'b7',
    isCaptured: false,
    changePieceTypeTo: null
  },
  bpc: {
    coord: 'c7',
    isCaptured: false,
    changePieceTypeTo: null
  },
  bpd: {
    coord: 'd7',
    isCaptured: false,
    changePieceTypeTo: null
  },
  bpe: {
    coord: 'e7',
    isCaptured: false,
    changePieceTypeTo: null
  },
  bpf: {
    coord: 'f7',
    isCaptured: false,
    changePieceTypeTo: null
  },
  bpg: {
    coord: 'g7',
    isCaptured: false,
    changePieceTypeTo: null
  },
  bph: {
    coord: 'h7',
    isCaptured: false,
    changePieceTypeTo: null
  },
  wpa: {
    coord: 'a2',
    isCaptured: false,
    changePieceTypeTo: null
  },
  wpb: {
    coord: 'b2',
    isCaptured: false,
    changePieceTypeTo: null
  },
  wpc: {
    coord: 'c2',
    isCaptured: false,
    changePieceTypeTo: null
  },
  wpd: {
    coord: 'd2',
    isCaptured: false,
    changePieceTypeTo: null
  },
  wpe: {
    coord: 'e2',
    isCaptured: false,
    changePieceTypeTo: null
  },
  wpf: {
    coord: 'f2',
    isCaptured: false,
    changePieceTypeTo: null
  },
  wpg: {
    coord: 'g2',
    isCaptured: false,
    changePieceTypeTo: null
  },
  wph: {
    coord: 'h2',
    isCaptured: false,
    changePieceTypeTo: null
  },
  wrq: {
    coord: 'a1',
    isCaptured: false
  },
  wnq: {
    coord: 'b1',
    isCaptured: false
  },
  wbq: {
    coord: 'c1',
    isCaptured: false
  },
  wq: {
    coord: 'd1',
    isCaptured: false
  },
  wk: {
    coord: 'e1',
    isCaptured: false
  },
  wbk: {
    coord: 'f1',
    isCaptured: false
  },
  wnk: {
    coord: 'g1',
    isCaptured: false
  },
  wrk: {
    coord: 'h1',
    isCaptured: false
  }
}

function setMoveData(move, data) {
  Object.entries(data).forEach(([key, val]) => (move[key] = val))
  if (data.fullMoveNumber && data.moveColor)
    move.halfMoveNumber =
      2 * data.fullMoveNumber - (data.moveColor === 'w' ? 2 : 1)
}

function setVariations(move, variations) {
  move.variations = variations
  for (const idx in move.variations) move.variations[idx].prev = move.prev
}

export function getFirstMove(move) {
  let currentMove = move
  while (true) {
    if (currentMove.prev == null) return currentMove
    else currentMove = currentMove.prev
  }
}

function getVariationStart(move) {
  let currentMove = move
  while (true) {
    if (currentMove.prev == null) return currentMove
    if (currentMove.prev.next.variations.includes(currentMove))
      return currentMove
    currentMove = currentMove.prev
  }
}

export function getLastMove(move) {
  let currentMove = move
  while (true) {
    if (currentMove.next == null) return currentMove
    else currentMove = currentMove.next
  }
}

export function getAllMoves(move) {
  let currentMove = getVariationStart(move)
  const moves = []
  while (currentMove != null) {
    moves.push(currentMove)
    currentMove = currentMove.next
  }
  return moves
}

function getPieceByCoord(position, coord) {
  return Object.keys(position).find((key) => {
    return position[key].coord === coord && !position[key].isCaptured
  })
}

function calculatePosition(oldPos, moveDescriptor) {
  const from = moveDescriptor.from()
  const to = moveDescriptor.to()
  const movingPiece = getPieceByCoord(oldPos, from)
  const newPos = cloneDeep(oldPos)
  newPos[movingPiece].coord = to
  if (moveDescriptor.isCapture() && !moveDescriptor.isEnPassant()) {
    const capturedPiece = getPieceByCoord(oldPos, to)
    newPos[capturedPiece].isCaptured = true
  }
  if (moveDescriptor.isEnPassant()) {
    const pawn = getPieceByCoord(oldPos, moveDescriptor.enPassantSquare())
    newPos[pawn].isCaptured = true
  }
  if (moveDescriptor.isCastling()) {
    const rook = getPieceByCoord(oldPos, moveDescriptor.rookFrom())
    newPos[rook].coord = moveDescriptor.rookTo()
  }
  if (moveDescriptor.isPromotion()) {
    // prettier-ignore
    newPos[movingPiece].changePieceTypeTo = moveDescriptor.coloredPromotion()
  }
  return newPos
}

export function extractMoveData(firstMoveKoko, startPos = undefined) {
  const isFromStart = !startPos
  let currentMove, firstMove
  if (isFromStart) {
    startPos = startPosition
    firstMove = { position: startPos }
    currentMove = { prev: firstMove }
    firstMove.next = currentMove
  } else {
    firstMove = {}
    currentMove = firstMove
  }

  let currentPos = startPos
  let currentMoveKoko = firstMoveKoko
  while (currentMoveKoko !== undefined) {
    setMoveData(currentMove, {
      fullMoveNumber: currentMoveKoko.fullMoveNumber(),
      nags: currentMoveKoko.nags(),
      comment: currentMoveKoko.comment(),
      san: currentMoveKoko.notation(),
      moveColor: currentMoveKoko.moveColor()
    })

    const newPos = calculatePosition(
      currentPos,
      currentMoveKoko._info.moveDescriptor
    )
    setMoveData(currentMove, { position: newPos })

    const variations = []
    currentMoveKoko.variations().forEach((variation) => {
      variations.push(extractMoveData(variation.first(), currentPos))
    })
    setVariations(currentMove, variations)

    if (currentMoveKoko.next()) {
      const newMove = { prev: currentMove }
      currentMove.next = newMove
      currentMove = newMove
    }

    currentPos = newPos
    currentMoveKoko = currentMoveKoko.next()
  }

  return firstMove
}
