import createPersistedState from 'vuex-persistedstate'
import { parse, stringify } from 'flatted'

export default ({ store }) => {
  window.onNuxtReady(() => {
    createPersistedState({
      paths: [
        'games.gameTabs',
        'games.gamesData',
        'games.queryResult',
        'games.queryResultLoadStatus',
        'text.locale'
      ],
      getState(key, storage, value) {
        console.log(value)
        return (value = storage.getItem(key)) && typeof value !== 'undefined'
          ? parse(value)
          : undefined
      },
      setState(key, state, storage) {
        return storage.setItem(key, stringify(state))
      }
    })(store)
  })
}
