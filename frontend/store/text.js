export const state = () => ({
  locale: 'en',
  layout: {
    en: {
      appBar: {
        featured: 'Featured Games',
        search: 'Search/Filter',
        upload: 'Upload Game'
      }
    },
    de: {
      appBar: {
        featured: 'Hervorgehobene Spiele',
        search: 'Suchen/Filtern',
        upload: 'Spiel hochladen'
      }
    }
  },
  index: {
    en: {
      heading: 'Welcome',
      description: `
        <p class="ma-0">
          This website provides two features:
        </p>
        <ul style="display: table; margin: 0 auto; text-align: left">
          <li><strong>View Games</strong></li>
          <li><strong>Output/Format games to pdf</strong></li>
        </ul>
        <p class="ma-0">
          First you open a game. You can either copy and paste the pgn text
          or you use the file explorer. You can also browse the featured games
          and the game database of nearly 1,000,000 games.
        </p>
      `,
      featured: 'Browse Featured Games',
      search: 'Search/Filter Game Database'
    },
    de: {
      heading: 'Willkommen',
      description: `
        <p class="ma-0">
          Auf dieser Webseite kann man zweierlei:
        </p>
        <ul style="display: table; margin: 0 auto; text-align: left">
          <li><strong>Spiele anschauen</strong></li>
          <li><strong>Spiele mit pdf-Datei als Output formatieren</strong></li>
        </ul>
        <p>
          Zunächst öffnest du ein Spiel, entweder durch kopieren
          und einfügen des pgn-Textes oder durch navigieren zur Datei.
          Ebenso kannst du die hervorgehobenen Spiele und die Datenbank
          von fast 1.000.000 Spielen durchstöbern.
        </p>
      `,
      featured: 'Hervorgehobene Spiele durchstöbern',
      search: 'Datenbank durchsuchen/filtern'
    }
  },
  uploadForm: {
    en: {
      title: 'Open chess game',
      textField: 'paste pgn',
      fileInput: 'browse for pgn',
      or: 'or',
      pgnFileValidation: {
        type: 'file type must be .pgn',
        size: 'file size must be max. 1MB'
      },
      wrongFileType: 'file type must be .pgn',
      openViewer: 'game viewer',
      openPrinter: 'pdf editor'
    },
    de: {
      title: 'Schachspiel öffnen',
      textField: 'pgn einfügen',
      fileInput: 'pgn datei öffnen',
      or: 'oder',
      pgnFileValidation: {
        type: 'Dateityp muss .pgn sein',
        size: 'Größe darf 1MB nicht überschreiten'
      },
      openViewer: 'Spiel Ansicht',
      openPrinter: 'pdf Editor'
    }
  },
  gameViewer: {
    en: {
      gameDataKeys: {
        opening: 'Opening',
        event: 'event',
        site: 'site',
        round: 'round',
        whiteElo: 'white elo',
        blackElo: 'black elo',
        annotator: 'annotator'
      }
    },
    de: {
      gameDataKeys: {
        opening: 'Eröffnung',
        event: 'Turnier',
        site: 'Ort',
        round: 'Runde',
        whiteElo: 'Weiß Elo',
        blackElo: 'Schwarz Elo',
        annotator: 'Annotiert von'
      }
    }
  },
  footer: {
    en: {
      privacy: 'Privacy Statement',
      impressum: 'Impressum (Legal Notice)'
    },
    de: {
      privacy: 'Datenschutzerklärung',
      impressum: 'Impressum'
    }
  },
  query: {
    en: {
      title: 'Search database for chess games'
    },
    de: {
      title: 'Durchsuche die Datenbank nach Schachspielen'
    }
  },
  queryForm: {
    en: {
      color: 'color',
      colorOptions: ['', 'white', 'black'],
      playerName: 'player name',
      result: 'result',
      event: 'event',
      site: 'site',
      whiteElo: 'white elo',
      blackElo: 'black elo',
      openingName: 'opening name',
      errors: {
        maxCharacter: 'maximum 64 characters',
        nameFormat: 'wrong name format',
        numberRequired: 'must be number',
        numberLength: 'maximum 4-digit number',
        differentColor: 'player colors must be different'
      },
      submit: 'submit',
      clear: 'clear'
    },
    de: {
      color: 'Farbe',
      colorOptions: ['', 'weiß', 'schwarz'],
      playerName: 'Spieler Name',
      result: 'Ergebnis',
      event: 'Veranstalltung',
      site: 'Ort',
      whiteElo: 'Weiß Elo',
      blackElo: 'Schwarz Elo',
      openingName: 'Name Eröffnung',
      errors: {
        maxCharacter: 'maximal 64 Buchstaben',
        nameFormat: 'falsches Namensformat',
        numberRequired: 'muss eine Zahl sein',
        numberLength: 'maximal 4 Ziffern',
        differentColor: 'Farben müssen verschieden sein'
      },
      submit: 'Suchen',
      clear: 'Leeren'
    }
  },
  queryResults: {
    en: {
      playerWhite: 'White',
      playerBlack: 'Black',
      result: 'Result',
      event: 'Event',
      opening: 'Opening',
      date: 'Date',
      tooltip: {
        view: 'view game',
        print: 'edit and print',
        download: 'download pgn'
      }
    },
    de: {
      playerWhite: 'Weiß',
      playerBlack: 'Schwarz',
      result: 'Ergebnis',
      event: 'Veranstalltung',
      opening: 'Eröffnung',
      date: 'Datum',
      tooltip: {
        view: 'anschauen',
        print: 'bearbeiten und drucken',
        download: 'pgn downloaden'
      }
    }
  },
  gamePrinter: {
    en: {
      heading: 'Printer Options',
      refreshBtn: 'refresh',
      positions: {
        heading: 'Printed Positions',
        positionsCheckbox: 'print position after',
        help:
          'Select the moves, after which you want the position shown in the pdf file.'
      },
      paragraphArr: {
        heading: 'Paragraph Arrangement',
        bmc: 'board-move-comment',
        mbc: 'move-board-comment',
        mcb: 'move-comment-board',
        help:
          'Choose in which order move (san), board and comment will be arranged.'
      },
      pageLayout: {
        heading: 'Page & Layout',
        pageMargin: 'page margin',
        colGap: 'column gap',
        help:
          'Setup the page with parge margin, gap between columns and page format.'
      },
      font: {
        heading: 'Font',
        size: 'size',
        help: 'Select your font type and size.'
      },
      colors: {
        heading: 'Board Colors',
        lightTile: 'light tile',
        darkTile: 'dark tile',
        help:
          'Change colors of dark and light tile colors in the board diagramms.'
      }
    },
    de: {
      heading: 'Drucker Optionen',
      refreshBtn: 'anwenden',
      positions: {
        heading: 'Angezeigte Positionen',
        positionsCheckbox: 'zeige Position nach',
        help: 'Wähle die Positionen aus, um sie im Pdf-Dokument abzubilden.'
      },
      paragraphArr: {
        heading: 'Paragraph Anordnung',
        bmc: 'Brett-Zug-Kommentar',
        mbc: 'Zug-Brett-Kommentar',
        mcb: 'Zug-Kommentar-Brett',
        help:
          'Ändere in welcher Ordnung Zug (san), Brett und Kommentar arrangiert werden.'
      },
      pageLayout: {
        heading: 'Seite & Layout',
        pageMargin: 'Seitenrand',
        colGap: 'Spaltenabstand',
        help: 'Richte die Seite mit Seitenrand, Spaltenabstand und Seitenformat'
      },
      font: {
        heading: 'Schrift',
        size: 'Größe',
        help: 'Wähle eine Schriftart und -größe.'
      },
      colors: {
        heading: 'Brett Farben',
        lightTile: 'weiße Felder',
        darkTile: 'schwarze Felder',
        help: 'Ändere die Farbe der dunklen und hellen Felder im Brettdiagramm.'
      }
    }
  },
  error: {
    en: 'Something went wrong.',
    de: 'Etwas ist schief gelaufen.'
  },
  featured: {
    en: {
      heading: 'Featured Games',
      tooltip: {
        view: 'view game',
        print: 'edit and print'
      }
    },
    de: {
      heading: 'Hervorgehobene Spiele',
      tooltip: {
        view: 'anschauen',
        print: 'bearbeiten und drucken'
      }
    }
  }
})

export const getters = {
  textChessLayout: (state) => state.layout[state.locale],
  textIndex: (state) => state.index[state.locale],
  textUploadForm: (state) => state.uploadForm[state.locale],
  textGameViewer: (state) => state.gameViewer[state.locale],
  textFooter: (state) => state.footer[state.locale],
  textQuery: (state) => state.query[state.locale],
  textQueryForm: (state) => state.queryForm[state.locale],
  textQueryResults: (state) => state.queryResults[state.locale],
  textGamePrinter: (state) => state.gamePrinter[state.locale],
  textError: (state) => state.error[state.locale],
  textFeatured: (state) => state.featured[state.locale]
}

export const mutations = {
  switchLocale: (state) => (state.locale = state.locale === 'en' ? 'de' : 'en')
}
