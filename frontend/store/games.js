import axios from 'axios'
import Vue from 'vue'
import {
  extractMoveData,
  getLastMove,
  getFirstMove
} from '@/plugins/pgnTools.js'
import kokopu from 'kokopu'

/* loadStatus:  0 -> Loading has not begun
                1 -> Loading has started
                2 -> Loading completed successfully
                3 -> Loading completed unsuccessfully */

// const featuredGameIds = [
//   // 969867,
//   969868,
//   969869,
//   969870,
//   969871,
//   969872,
//   969873,
//   969874,
//   969875,
//   969876,
//   969877,
//   969878,
//   969879,
//   969880,
//   969881
// ]

const featuredGameIds = [969869, 969873, 969874, 969877]

export const state = () => ({
  // currentGame is the selected game for gameViewer and chessPrintTool
  featuredGameIds: [],
  queryResult: {
    foundGames: 0,
    pageSize: 10,
    pageNumber: 1,
    pageTotal: 0,
    games: [],
    query: {}
  },
  queryResultLoadStatus: 0,
  gameTabs: [
    // { id, role }
  ],
  // the key for a game corresponds to id of game in db
  gamesData: {
    // EXAMPLE
    // 1: {
    //   id: Number,
    //   event: String,
    //   site: String,
    //   date: String,
    //   round: String,
    //   result: String,
    //   whitePlayerId: Number,
    //   whitePlayer: String,
    //   blackPlayerId: Number,
    //   blackPlayer: String,
    //   whiteElo: Number,
    //   blackElo: Number,
    //   annotator: String,
    //   moves: Array,
    //   opening: Object,
    //   openingLoadStatus: Number,
    //   loadStatus: Number,
    //   currentMove: 0,
    //   boardIsFlipped: false,
    //   playerPictureUrls: {
    //     white: undefined,
    //     whiteName: undefined,
    //     black: undefined,
    //     blackName: undefined
    //   },
    //   playerPictureUrlsLoadStatus: 0,
    //   pdf: String
    // }
  }
})

export const actions = {
  async fetchFeaturedGames({ commit, dispatch }) {
    for (const id of featuredGameIds) {
      await dispatch('processGameData', { id, noMoves: true })
      commit('addFeaturedGameId', id)
    }
  },

  async fetchQueryResult({ commit }, queryParams) {
    commit('setQueryResultLoadStatus', 1)
    try {
      var response = await this.$axios.get('/api/game', {
        params: queryParams
      })
    } catch (err) {
      console.log(err)
      commit('setQueryResultLoadStatus', 3)
    }
    const queryResult = response.data
    queryResult.query = queryParams
    commit('setQueryResult', queryResult)
    commit('setQueryResultLoadStatus', 2)
  },

  addGameTab({ commit, state }, { id, role }) {
    const tabAlreadyExists = state.gameTabs.some(
      (tab) => tab.id === id && tab.role === role
    )
    if (!tabAlreadyExists) commit('addGameTab', { id, role })
  },

  async processGameData({ commit, dispatch, state }, { id, pgn, noMoves }) {
    // check for valid function call
    if (id && pgn) throw new Error('can only have id xor pgn')
    if (pgn && noMoves)
      throw new Error("move parsing can't be ommited with custom games")

    // hashed-pgn-id must mean the data is already processed
    const isHashedPgn = !pgn && isNaN(id)
    if (isHashedPgn) return

    // Make some boolean variables for code flow control
    let isFromUser
    if (pgn) {
      id = this._vm.$hashPgn(pgn)
      isFromUser = true
    } else {
      isFromUser = false
    }
    const dataAlreadyExists = Boolean(state.gamesData[id])
    const movesExist = dataAlreadyExists && state.gamesData[id].moves
    if (
      (dataAlreadyExists && movesExist) ||
      (dataAlreadyExists && noMoves && !movesExist)
    )
      return

    // set load status
    commit('setLoadStatus', { id, status: 1 })

    // make api call if needed
    let meta
    if (!isFromUser) {
      try {
        const response = await this.$axios.get(`/api/game/${id}`)
        meta = response.data
        pgn = response.data.moveText
      } catch (err) {
        commit('setLoadStatus', { id, status: 3 })
        console.log(err)
        return
      }
    }

    // parse pgn if noMoves isn't set to true
    let moves, parsedPgn
    if (!noMoves) {
      try {
        parsedPgn = kokopu.pgnRead(pgn).game(0)
      } catch (err) {
        commit('setLoadStatus', { id, status: 3 })
        console.log(err)
        return
      }
      moves = extractMoveData(parsedPgn.mainVariation().first())
      commit('setCurrentMove', { id, role: 'viewer', currentMove: moves })
      commit('setCurrentMove', { id, role: 'printer', currentMove: moves })
    }

    // if data was already there and moves didn't exist only add them
    if (dataAlreadyExists && !movesExist) {
      commit('addGameData', {
        id,
        gameData: {
          moves
        }
      })
      commit('setLoadStatus', { id, status: 2 })
      return
    }

    // commit game data
    if (!isFromUser) {
      // concatenate player objects to single string
      let whitePlayer = meta.players[0]
      let blackPlayer = meta.players[1]
      whitePlayer =
        whitePlayer.middleNames != null
          ? `${whitePlayer.firstName} ${whitePlayer.middleNames} ${whitePlayer.lastName}`
          : `${whitePlayer.firstName} ${whitePlayer.lastName}`
      blackPlayer =
        blackPlayer.middleNames != null
          ? `${blackPlayer.firstName} ${blackPlayer.middleNames} ${blackPlayer.lastName}`
          : `${blackPlayer.firstName} ${blackPlayer.lastName}`

      commit('addGameData', {
        id,
        gameData: {
          id,
          meta: {
            event: meta.event,
            site: meta.site,
            date: meta.date,
            round: meta.matchRound,
            result: meta.result,
            annotator: meta.annotator,
            white: {
              id: meta.players[0].id,
              name: whitePlayer,
              elo: meta.whiteElo
            },
            black: {
              id: meta.players[1].id,
              name: blackPlayer,
              elo: meta.blackElo
            }
          },
          moves,
          pdfUrl: '',
          pdfLoadStatus: 0
        }
      })
      commit('setLoadStatus', { id, status: 2 })
    } else {
      // prevent errors reading date
      let date
      if (parsedPgn._date !== undefined) {
        const dateReturn = parsedPgn.date()
        if (dateReturn.toLocaleDateString !== undefined) {
          date = dateReturn.toLocaleDateString()
        } else {
          date = dateReturn.year // maybe undefined
        }
      } else {
        date = undefined
      }

      commit('addGameData', {
        id,
        gameData: {
          id,
          meta: {
            event: parsedPgn.event(),
            site: parsedPgn.site(),
            date,
            round: parsedPgn.round(),
            result: parsedPgn.result(),
            annotator: parsedPgn.annotator(),
            white: {
              name: parsedPgn.playerName('w'),
              elo: parsedPgn.playerElo('w')
            },
            black: {
              name: parsedPgn.playerName('b'),
              elo: parsedPgn.playerElo('b')
            }
          },
          moves,
          pgn,
          pdfUrl: '',
          pdfLoadStatus: 0
        }
      })
      commit('setLoadStatus', { id, status: 2 })
    }

    // handle player pictures
    dispatch('fetchPlayerPictureUrls', id)

    // handle opening
    if (isFromUser) dispatch('fetchOpening', { id, pgn })
    else {
      commit('setOpening', { id, opening: meta.opening })
      commit('setOpeningLoadStatus', { id, status: 2 })
    }
  },

  async fetchOpening({ commit }, { id, pgn }) {
    commit('setOpeningLoadStatus', { id, status: 1 })
    try {
      const response = await this.$axios.post('/api/opening/', {
        pgn
      })
      commit('setOpening', { id, opening: response.data })
      commit('setOpeningLoadStatus', { id, status: 2 })
    } catch (err) {
      commit('setOpeningLoadStatus', { id, status: 3 })
      console.log(err)
    }
  },

  async fetchPlayerPictureUrls({ commit, state }, id) {
    if (state.gamesData[id].loadStatus !== 2) return
    commit('setPlayerPictureUrlsLoadStatus', { id, status: 1 })
    const meta = state.gamesData[id].meta
    const playerNames = {
      white: meta.white.name ? meta.white.name : '',
      black: meta.black.name ? meta.black.name : ''
    }
    try {
      var pageIds = {}
      for (const [color, name] of Object.entries(playerNames)) {
        const response = await axios.get(
          'https://en.wikipedia.org/w/api.php?',
          {
            params: {
              origin: '*',
              action: 'query',
              format: 'json',
              list: 'search',
              srsearch: name,
              srlimit: 1
            }
          }
        )
        if (
          response.data.query.searchinfo.totalhits > 0 &&
          /* check if title conatins any of the name party */
          name
            .split(' ')
            .some((namePart) =>
              response.data.query.search[0].title.includes(namePart)
            )
        ) {
          pageIds[color] = response.data.query.search[0].pageid
        }
      }
      var response = await this.$axios.get(
        'https://en.wikipedia.org/w/api.php?',
        {
          params: {
            origin: '*',
            action: 'query',
            format: 'json',
            prop: 'pageimages',
            piprop: 'original',
            pageids: `${pageIds.white}|${pageIds.black}`
          }
        }
      )
    } catch (err) {
      commit('setPlayerPictureUrlsLoadStatus', { id, status: 3 })
      console.log(err)
      return
    }
    try {
      var white = response.data.query.pages[pageIds.white].original.source
    } catch (err) {
      white = ''
    }
    try {
      var black = response.data.query.pages[pageIds.black].original.source
    } catch (err) {
      black = ''
    }
    /* if both pictures don't work out */
    if (!white && !black) {
      commit('setPlayerPictureUrlsLoadStatus', { id, status: 3 })
      return
    }
    commit('setPlayerPictureUrls', {
      id,
      urls: {
        white,
        black
      }
    })
    commit('setPlayerPictureUrlsLoadStatus', { id, status: 2 })
  },

  async fetchPdf({ commit, state }, { id, options }) {
    commit('setPdfLoadStatus', { id, status: 1 })
    const isFromUser = isNaN(id)
    if (isFromUser) options.pgn = state.gamesData[id].pgn
    else options.gameId = id

    let response
    try {
      response = await this.$axios.post('api/gamePrinter/', options)
    } catch (err) {
      commit('setPdfLoadStatus', { id, status: 3 })
      console.log(err)
      return
    }
    const oldPdfUrl = state.gamesData[id].pdfUrl
    if (oldPdfUrl) window.URL.revokeObjectURL(oldPdfUrl)
    const pdfBlob = new Blob([response.data], { type: 'application/pdf' })
    const pdfUrl = window.URL.createObjectURL(pdfBlob)
    commit('setPdfUrl', { id, pdfUrl })
    commit('setPdfLoadStatus', { id, status: 2 })
  },

  incCurrentMove({ commit, state }, { id, role }) {
    if (state.gamesData[id].loadStatus !== 2) return
    const currentMove = state.gamesData[id].currentMove[role]
    if (currentMove.next)
      commit('setCurrentMove', { id, role, currentMove: currentMove.next })
  },

  decCurrentMove({ commit, state }, { id, role }) {
    if (state.gamesData[id].loadStatus !== 2) return
    const currentMove = state.gamesData[id].currentMove[role]
    if (currentMove.prev)
      commit('setCurrentMove', { id, role, currentMove: currentMove.prev })
  },

  setCurrentMove({ commit, state }, { id, role, newCurrentMove }) {
    if (state.gamesData[id].loadStatus !== 2) return
    commit('setCurrentMove', { id, role, currentMove: newCurrentMove })
  },

  startCurrentMove({ commit, state }, { id, role }) {
    if (state.gamesData[id].loadStatus !== 2) return
    const currentMove = state.gamesData[id].currentMove[role]
    commit('setCurrentMove', {
      id,
      role,
      currentMove: getFirstMove(currentMove)
    })
  },

  endCurrentMove({ commit, state }, { id, role }) {
    if (state.gamesData[id].loadStatus !== 2) return
    const currentMove = state.gamesData[id].currentMove[role]
    commit('setCurrentMove', {
      id,
      role,
      currentMove: getLastMove(currentMove)
    })
  }
}

export const mutations = {
  addFeaturedGameId: (state, gameId) =>
    Vue.set(state.featuredGameIds, state.featuredGameIds.length, gameId),
  setQueryResult: (state, queryResult) => (state.queryResult = queryResult),
  setQueryResultLoadStatus: (state, status) =>
    (state.queryResultLoadStatus = status),
  addGameTab: (state, { id, role }) => {
    const alreadyExists = state.gameTabs.some(
      (tab) => tab.id === id && tab.role === role
    )
    if (!alreadyExists)
      Vue.set(state.gameTabs, state.gameTabs.length, { id, role })
  },
  removeGameTab: (state, { id, role }) => {
    const isThisTab = (tab) => tab.id === id && tab.role === role
    state.gameTabs = state.gameTabs.filter((tab) => !isThisTab(tab))
    const sameIdTabExists = state.gameTabs.some((tab) => tab.id === id)
    const isFeatured = featuredGameIds.some(
      (featuredGameId) => featuredGameId === parseInt(id)
    )
    if (!sameIdTabExists && !isFeatured) delete state.gamesData[id]
  },
  addGameData: (state, { id, gameData }) => {
    if (!state.gamesData[id]) Vue.set(state.gamesData, id, {})
    Vue.set(
      state.gamesData,
      id,
      Object.assign({}, state.gamesData[id], gameData)
    )
  },
  setPdfUrl: (state, { id, pdfUrl }) => {
    Vue.set(
      state.gamesData,
      id,
      Object.assign({}, state.gamesData[id], { pdfUrl })
    )
  },
  setPdfLoadStatus: (state, { id, status }) => {
    if (!state.gamesData[id]) Vue.set(state.gamesData, id, {})
    Vue.set(state.gamesData[id], 'pdfLoadStatus', status)
  },
  setLoadStatus: (state, { id, status }) => {
    if (!state.gamesData[id]) Vue.set(state.gamesData, id, {})
    Vue.set(state.gamesData[id], 'loadStatus', status)
  },
  setCurrentMove: (state, { id, role, currentMove }) => {
    if (state.gamesData[id]) {
      if (!state.gamesData[id].currentMove)
        Vue.set(state.gamesData[id], 'currentMove', {})
      Vue.set(state.gamesData[id].currentMove, role, currentMove)
    }
  },
  setBoardIsFlipped: (state, { id, role }) => {
    if (state.gamesData[id]) {
      if (!state.gamesData[id].boardIsFlipped)
        Vue.set(state.gamesData[id], 'boardIsFlipped', {})
      Vue.set(
        state.gamesData[id].boardIsFlipped,
        role,
        !state.gamesData[id].boardIsFlipped[role]
      )
    }
  },
  setOpening: (state, { id, opening }) => {
    if (state.gamesData[id])
      Vue.set(state.gamesData[id].meta, 'opening', opening)
  },
  setOpeningLoadStatus: (state, { id, status }) => {
    if (state.gamesData[id])
      Vue.set(state.gamesData[id].meta, 'openingLoadStatus', status)
  },
  setPlayerPictureUrls: (state, { id, urls }) => {
    if (state.gamesData[id])
      Vue.set(state.gamesData[id].meta, 'playerPictureUrls', urls)
  },
  setPlayerPictureUrlsLoadStatus: (state, { id, status }) => {
    if (state.gamesData[id])
      Vue.set(state.gamesData[id].meta, 'playerPictureUrlsLoadStatus', status)
  }
}
